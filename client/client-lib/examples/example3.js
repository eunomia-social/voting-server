const lib = require("../lib.js");

const NODE_ENDPOINT = "http://146.193.69.131:5000/";
const VOTING_ENDPOINT = "http://146.193.69.131:8100/";
const ACCESS_TOKEN = "9PxsxQrpf5Q9ybD4E5BKi35wr6vb9OentFQkjQUU8mRH8LNma3";
const USER_ID = "867263"; // must be a valid SN user
const REGISTRATION = false; // 867263 is already registered (on test node)
const SECRET = "qwerty"; // secret for user 867263

// other parameters
const POST_ID = "ab241b8";

console.log("Initializing and calling hasVoted...");
lib.initialize(NODE_ENDPOINT,VOTING_ENDPOINT, ACCESS_TOKEN, USER_ID, REGISTRATION, SECRET).then(response => {
    // log response to ensure everything is ok
    console.log(response);

    // ask the voting server if the user has voted already
    // or check locally
    return lib.hasVoted(USER_ID, POST_ID);
}).then(response => {
    // handle hasVoted response here
    console.error(response);
}).catch(error => {
    // handle an error here
    console.error(error);
});

// sleep timer
setTimeout(function() {
    console.log(`Getting tally for post ${POST_ID}...`);
    lib.tally(NODE_ENDPOINT, VOTING_ENDPOINT, ACCESS_TOKEN, POST_ID).then(response => {
        // returns tallying results (check readme example)
        console.log(response);
    }).catch(error => {
        // handle tally error here
        console.error(error);
    });
}, 5000);

// sleep timer
setTimeout(function() {
    // clear all information related to a user
    console.log(`Clearing information for user ${USER_ID}...`);
    lib.clear(USER_ID).then(response => {
        // states that the operation completed successfully
        console.log(response);
    }).catch(error => {
        // handle the clear error here
        console.error(error);
    });
}, 10000);