const lib = require("../lib.js");

const NODE_ENDPOINT = "http://127.0.0.1:5000/";
const VOTING_ENDPOINT = "http://127.0.0.1:8100/";
const ACCESS_TOKEN = "HpR27Y4120jL07W005J6Q1UQoapG24f0pH3OH2eNK9RcXwOH1e";
const USER_ID = "1293750"; // must be a valid SN user
const REGISTRATION = true; // 867263 is already registered (on test node)
const SECRET = "qwerty"; // secret for user 867263

// other parameters
const POST_ID = "ab241b8";
const POST_ID_2 = "ab23422";

let votingBody = {};

console.log("Initializing...");
lib.initialize(NODE_ENDPOINT, VOTING_ENDPOINT, ACCESS_TOKEN, USER_ID, REGISTRATION, SECRET).then(response => {
    console.log(JSON.stringify(response));
}).catch(error => {
    // handle the initialization error here
    console.error(error);
});

// sleep timer
setTimeout(function() {
    console.log(`Getting ballot for post ${POST_ID}...`);
    lib.getBallot(ACCESS_TOKEN, POST_ID, USER_ID).then(response => {
        // log response to ensure everything is ok
        console.log(response);

        // these are presented to the user
        let ballot = response.message.ballot;
        let features = response.message.features;

        // the user updates the ballot using the DC UI
        // here we just manipulate the ballot variable
        // let's attribute a value of 10 to the trust tag of the ballot
        // and a value of -5 to the no_trust tag of the ballot
        ballot.votes.trust = 10;
        ballot.votes.no_trust = -5;

        // the same thing happens with the features: we let the
        // user select which ones to append to the ballot
        // let's suppose the user wants only to share his followers,
        // we delete the politics tag
        delete features.features.politics;

        // before calling the vote function, we have to create an object
        // that has both the ballot and the features
        votingBody = {
            "ballot": ballot,
            "features": features
        };
    }).catch(error => {
        // handle the getBallot error here
        console.error(error);
    });
}, 3000);

// sleep timer
setTimeout(function() {
    // now everything is set for the ballot to be added to the tally
    console.log("Voting...");
    lib.vote(ACCESS_TOKEN, USER_ID, votingBody, false).then(response => {
        console.log(JSON.stringify(response));
    }).catch(error => {
        // handle the voting error here
        console.error(error);
    });
}, 6000);

//// sleep timer
//setTimeout(function() {
//    console.log(`Getting ballot for post ${POST_ID_2}...`);
//    lib.getBallot(ACCESS_TOKEN, POST_ID_2, USER_ID).then(response => {
//        // log response to ensure everything is ok
//        console.log(response);
//
//        // these are presented to the user
//        let ballot = response.message.ballot;
//        let features = response.message.features;
//
//        // the user updates the ballot using the DC UI
//        // here we just manipulate the ballot variable
//        // let's attribute a value of 10 to the trust tag of the ballot
//        // and a value of -5 to the no_trust tag of the ballot
//        ballot.votes.trust = 20;
//        ballot.votes.no_trust = -5;
//
//        // the same thing happens with the features: we let the
//        // user select which ones to append to the ballot
//        // let's suppose the user wants only to share his followers,
//        // we delete the politics tag
//        delete features.features.politics;
//
//        // before calling the vote function, we have to create an object
//        // that has both the ballot and the features
//        votingBody = {
//            "ballot": ballot,
//            "features": features
//        };
//    }).catch(error => {
//        // handle the getBallot error here
//        console.error(error);
//    });
//}, 9000);

// sleep timer
setTimeout(function() {
    console.log(`Getting tally for post ${POST_ID}...`);
    lib.tally(NODE_ENDPOINT, VOTING_ENDPOINT, ACCESS_TOKEN, POST_ID).then(response => {
        // returns tallying results (check readme example)
        console.log(JSON.stringify(response));
    }).catch(error => {
        // handle tally error here
        console.error(error);
    });
}, 12000);

// sleep timer
setTimeout(function() {
    console.log(`Getting tallies for post ${POST_ID} ${POST_ID_2}...`);
    lib.tallies(NODE_ENDPOINT, VOTING_ENDPOINT, ACCESS_TOKEN, [POST_ID,POST_ID_2]).then(response => {
        // returns tallying results (check readme example)
        console.log(JSON.stringify(response));
    }).catch(error => {
        // handle tally error here
        console.error(error);
    });
}, 15000);
