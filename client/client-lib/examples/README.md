## Client Library Examples

In this folder, there are 3 examples that show how to properly integrate the Javascript library with the Digital 
Companion.

The first thing to do is to import the library. Remember to change the directory to the path where the *lib.js* 
file is located.

```
const lib = require("./lib.js");
```

Other important aspects of all examples are the constants declared at the beginning:

```
const NODE_ENDPOINT = "http://146.193.69.131:5000/";
const VOTING_ENDPOINT = "http://146.193.69.131:8100/";
const ACCESS_TOKEN = "9PxsxQrpf5Q9ybD4E5BKi35wr6vb9OentFQkjQUU8mRH8LNma3";
const USER_ID = "867263";
const REGISTRATION = false;
const SECRET = "qwerty";
```

They exist for the sake of these examples since they can change during the process. Both of them need to be updated to 
valid values before running any of the example files.

To run an example, let node.js interpret the file by executing the following command:

```
node exampleX.js
```

### Library Methods

The client library has the following methods to be called that handle all the functionality related to 
the voting feature of EUNOMIA. This list summarizes what each of them does. For more detailed information
such as parameters and return values, check the *JSDoc* descriptions for these functions.

- **initialize**: Configures the library to work properly with user/voter operations such as getting a ballot or 
voting. Required whenever there's a new user logging in or signing up.
- **getBallot**: Requests a ballot to the voting server, along with the user features (number of followers, ...) 
that can be appended to it. (REQUIRES initialization)
- **vote**: Votes on a post. (REQUIRES initialization)
- **hasVoted**: Asks the voting server if the particular voter has voted before on a certain post. If the user has
voted before, the ballot is also returned. This information may also exist locally. (REQUIRES initialization)
- **tally**: Requests the tally of a post to the voting server. 
- **clear**: Clears the browser's local storage or deletes the specified keys passed as arguments. When a user logs
out, **it is important that his private key is not preserved**. As so, simply call the clear method and provide 
the user identifier as an argument.

### Handling Returned Objects

The client library always returns an object/dictionary of the same format that has 3 keys: **code**, **message** and 
**error**. The code only exists if the success/error is related to a web request. The message key always exists and 
contains either the response (payload) returned by the library (or voting/discovery/storage server) or the origin
of an error. Finally, the error key only exists if there was an error and states the error message.

Below are examples that show distinct situations for successful and erroneous operations.

#### Examples

##### Successful Return 1 (getBallot)

```json
{
  "code": 200,
  "message": {
    "ballot": {
      "post_id": "ab241b8",
      "votes": {
        "trust": 0,
        "no_trust": 0
      }
    },
    "features": {
      "user_id": "123456789",
      "features": {
        "followers": 12345,
        "politics": "republican"
      }
    } 
  }
}
```

##### Successful Return 2 (tally)

```json
{
  "code": 200,
  "message": {
    "post_id": "ab241b8",
    "results": [
      {
        "votes": {
          "trust": 1,
          "no_trust": 0
        },
        "features": {
          "followers": 1234
        }
      },
      {
        "votes": {
          "trust": 0,
          "no_trust": 1
        }
      }
    ]
  }
}
```

##### Error (initialization)

```json
{
  "message": "Error: Client Library",
  "error": "Voting Server with ID 01b850f6-7fe8-11ea-8526-abcb246654f4 is not active"
}
```

### Code Examples

#### Example 1

In this example, the 3 operations required to vote (initialize, getBallot and vote) are chained together using 
promises, so when one of them finishes, the next one is called immediately after. There is also a **catch** at the 
end of the chain should there be any errors along the way, which can be handled the way the programmer desires (the 
example simply logs the error).

This example is quite unrealistic as the library operations are never carried out successively. It exists merely to 
check that the library is running correctly.

#### Example 2

This example is more realistic as it portraits the usual scenario where calls to the library are made and handled 
separately. There is a sleep timer of 5 seconds between calls to avoid the start of the following promise without 
the previous one having finished (and still there is no 100% assurance this won't happen) and simulate a real 
situation where the user intervenes between the calls.

#### Example 3

This last example presents the use of the tally, hasVoted and clear operations. Similar to previous example, these
operations are not chained but rather separated by sleep timers, except for the initialize + hasVoted operation.