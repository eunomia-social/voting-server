const lib = require("../lib.js");

const NODE_ENDPOINT = "http://146.193.69.131:5000/";
const VOTING_ENDPOINT = "http://146.193.69.131:8100/";
const ACCESS_TOKEN = "9PxsxQrpf5Q9ybD4E5BKi35wr6vb9OentFQkjQUU8mRH8LNma3";
const USER_ID = "867263"; // must be a valid SN user
const REGISTRATION = false; // 867263 is already registered (on test node)
const SECRET = "qwerty"; // secret for user 867263

// other parameters
const POST_ID = "ab241b8";

lib.initialize(NODE_ENDPOINT, VOTING_ENDPOINT, ACCESS_TOKEN, USER_ID, REGISTRATION, SECRET).then(response => {
    // log response to ensure everything is ok
    console.log(response);

    // next step is to request a ballot for some random post
    return lib.getBallot(ACCESS_TOKEN, POST_ID, USER_ID);
}).then(response => {
    // log response to ensure everything is ok
    console.log(response);

    // these are presented to the user
    let ballot = response.message.ballot;
    let features = response.message.features;

    // the user updates the ballot using the DC UI
    // here we just manipulate the ballot variable
    // let's attribute a value of 10 to the trust tag of the ballot
    // and a value of -5 to the no_trust tag of the ballot
    ballot.votes.trust = 10;
    ballot.votes.no_trust = -5;

    // the same thing happens with the features: we let the
    // user select which ones to append to the ballot
    // let's suppose the user wants only to share his followers,
    // we delete the politics tag
    delete features.features.politics;

    // before calling the vote function, we have to create an object
    // that has both the ballot and the features
    let body = {
        "ballot": ballot,
        "features": features
    };

    // now everything is set for the ballot to be added to the tally
    return lib.vote(ACCESS_TOKEN, USER_ID, body, false);
}).then(response => {console.log("1"); console.log(response)})
.catch(error => {console.log("2"); console.error(error); console.trace();});