'use strict';

const utils = require('../utils/writer.js');
const library = require('../../client-lib/lib.js');

module.exports.initialize = function initialize(req, res, next) {
    let endpoint = req.swagger.params['endpoint'].value;
    let access_token = req.swagger.params['access_token'].value;
    let user_id = req.swagger.params['user_id'].value;
    let body = req.swagger.params['body'].value;

    let secret = ("secret" in body) ? body.secret : null;

    library.initialize(endpoint, access_token, user_id, secret).then(response => {
        utils.writeJson(res, response);
    }).catch(error => {
        utils.writeJson(res, error);
    });
};

module.exports.getBallot = function getBallot(req, res, next) {
    let postId = req.swagger.params['post_id'].value;
    let userId = req.swagger.params['user_id'].value;
    let accessToken = req.swagger.params['access_token'].value;

    library.getBallot(accessToken, postId, userId).then(response => {
        utils.writeJson(res, response);
    }).catch(error => {
        utils.writeJson(res, error);
    });
};

module.exports.vote = function vote(req, res, next) {
    let accessToken = req.swagger.params['access_token'].value;
    let userId = req.swagger.params['user_id'].value;
    let anonymous = req.swagger.params['anonymous'].value;
    let body = req.swagger.params['body'].value;

    library.vote(accessToken, userId, body, anonymous).then(response => {
        utils.writeJson(res, response);
    }).catch(error => {
        utils.writeJson(res, error);
    });
};
