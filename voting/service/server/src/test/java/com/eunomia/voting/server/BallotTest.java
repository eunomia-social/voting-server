package com.eunomia.voting.server;

import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

/**
 * Defines a batch of unit tests for the GET API Call <b>/ballot</b>.
 */
public class BallotTest {

    // set API call path
    private static final String RESOURCE = "voting/ballot";

    // valid parameters
    private static final String POST_ID = "123";

    // invalid parameters
    private static final String INVALID_POST_ID = "0";

    @Test
    public void missingPostId() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request().get();

        // test response
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void invalidPostId() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .queryParam("post_id", INVALID_POST_ID)
                .request().get();

        // test response
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void validRequest() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .queryParam("post_id", POST_ID)
                .request().get();

        // test response
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

}
