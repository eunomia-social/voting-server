package com.eunomia.voting.server;

import com.eunomia.voting.components.Admin;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

/**
 * Defines a batch of unit tests for the POST API Call <b>/anonymize</b>.
 */
public class AnonymizeTest {

    // set API call path
    private static final String RESOURCE = "voting/anonymize";

    // valid parameters
    private static final String ACCESS_TOKEN = Admin.getAccessToken();

    // invalid parameters
    private static final String INVALID_ACCESS_TOKEN = "123456789";

    @Test
    public void missingToken() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity("body", MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void invalidToken() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .queryParam("access_token", INVALID_ACCESS_TOKEN)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity("body", MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
    }
}
