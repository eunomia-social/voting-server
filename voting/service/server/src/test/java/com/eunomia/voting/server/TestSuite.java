package com.eunomia.voting.server;

import com.eunomia.voting.components.Admin;
import com.eunomia.voting.library.Constants;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * This test suite sets up and tears down the unit testing process
 * for the voting server and runs the unit tests declared and
 * implemented in this package.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({AnonymizeTest.class, BallotTest.class, CreateVoterTest.class, EligibilityTest.class,
        FeaturesTest.class, GetVoterTest.class, TallyTest.class, VoteTest.class})
public class TestSuite {

    // REMEMBER THIS WHEN DEVELOPING TESTS
    // When testing a REST resource, there are usually a few orthogonal responsibilities the tests should focus on:
    //
    // - The HTTP response code
    // - Other HTTP headers in the response
    // - The payload (JSON, XML)
    //
    // Each test should only focus on a single responsibility and include a single assertion. Focusing on a clear
    // separation always has benefits, but when doing this kind of black box testing is even more important, as
    // the general tendency is to write complex test scenarios in the very beginning.

    protected static HttpServer server;
    protected static WebTarget target;

    @BeforeClass
    public static void setUp() throws Exception {
        // build uri
        final URI uri = UriBuilder.fromUri(Constants.API_BASE_URI).port(Constants.PORT_NUMBER).path(Constants.API_PATH).build();

        // start server
        server = VotingServer.start(uri);

        // initialize voting service
        if (!Admin.initialize()) {
            System.out.println("Initialization process failed.");
            System.exit(1);
        }

        // create the client
        Client c = ClientBuilder.newClient();

        // uncomment the following line if you want to enable
        // support for JSON in the client (you also have to uncomment
        // dependency on jersey-media-json module in pom.xml and Main.startServer())
        // c.getConfiguration().enable(new org.glassfish.jersey.media.json.JsonJaxbFeature());

        target = c.target(uri);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        Admin.shutdown();
        server.shutdownNow();
    }
}
