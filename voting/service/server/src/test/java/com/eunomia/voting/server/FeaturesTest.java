package com.eunomia.voting.server;

import com.eunomia.voting.components.Admin;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

/**
 * Defines a batch of unit tests for the GET API Call <b>/features</b>.
 */
public class FeaturesTest {

    // set API call path
    private static final String RESOURCE = "voting/features";

    // valid parameters
    private static final String ACCESS_TOKEN = Admin.getAccessToken();
    private static final String USER_ID = "123";

    // invalid parameters
    private static final String INVALID_ACCESS_TOKEN = "123456789";
    private static final String FORBIDDEN_USER_ID = "0";
    private static final String INVALID_USER_ID = "-1";

    @Test
    public void missingToken() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .queryParam("user_id", USER_ID)
                .request().get();

        // test response
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void missingUserId() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .queryParam("access_token", ACCESS_TOKEN)
                .request().get();

        // test response
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void invalidToken() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .queryParam("access_token", INVALID_ACCESS_TOKEN)
                .queryParam("user_id", USER_ID)
                .request().get();

        // test response
        assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
    }

    @Test
    public void invalidUserId() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .queryParam("access_token", ACCESS_TOKEN)
                .queryParam("user_id", INVALID_USER_ID)
                .request().get();

        // test response
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void forbiddenUserId() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .queryParam("access_token", ACCESS_TOKEN)
                .queryParam("user_id", FORBIDDEN_USER_ID)
                .request().get();

        // test response
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void validRequest() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .queryParam("access_token", ACCESS_TOKEN)
                .queryParam("user_id", USER_ID)
                .request().get();

        // test response
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

}
