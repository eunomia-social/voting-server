package com.eunomia.voting.server;

import com.eunomia.voting.library.RequestHelper;
import com.eunomia.voting.models.Ballot;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * Defines a batch of unit tests for the POST API Call <b>/vote</b>.
 */
public class VoteTest {

    // set API call path
    private static final String RESOURCE = "voting/vote";

    // valid parameters
    private static final String POST_ID = "123";
    private static final HashMap<String, Object> VOTES = new HashMap<>() {{
        put("trust", 5);
        put("no_trust", 1);
    }};

    // invalid parameters
    private static final String INVALID_POST_ID = "0";
    private static final HashMap<String, Object> INVALID_VOTES = new HashMap<>() {{
        put("0", 0);
        put("1", 1);
    }};

    @Test
    public void missingBody() {
        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(null, MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    /*

    @Test
    public void missingBallot() {
        // build body
        RequestHelper.VoteRequest body = new RequestHelper.VoteRequest();
        body.setSignatures(new HashMap<>());

        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(body, MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void missingSignatures() {
        // build body
        RequestHelper.VoteRequest body = new RequestHelper.VoteRequest();
        body.setBallot(new Ballot(POST_ID, COMMITMENT, VOTES));

        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(body, MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void malformedBallot() {
        // build body
        RequestHelper.VoteRequest body = new RequestHelper.VoteRequest();
        body.setBallot(new Ballot(POST_ID, VOTES));

        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(body, MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void invalidPostId() {
        // build body
        RequestHelper.VoteRequest body = new RequestHelper.VoteRequest();
        body.setBallot(new Ballot(INVALID_POST_ID, COMMITMENT, VOTES));

        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(body, MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void invalidCommitment() {
        // build body
        RequestHelper.VoteRequest body = new RequestHelper.VoteRequest();
        body.setBallot(new Ballot(POST_ID, INVALID_COMMITMENT, VOTES));

        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(body, MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void invalidVotes() {
        // build body
        RequestHelper.VoteRequest body = new RequestHelper.VoteRequest();
        body.setBallot(new Ballot(POST_ID, COMMITMENT, INVALID_VOTES));

        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(body, MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void invalidSignatures() {
        // build body
        RequestHelper.VoteRequest body = new RequestHelper.VoteRequest();
        body.setBallot(new Ballot(POST_ID, COMMITMENT, VOTES));

        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(body, MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Test
    public void validRequest() {
        // build body
        RequestHelper.VoteRequest body = new RequestHelper.VoteRequest();
        body.setBallot(new Ballot(POST_ID, COMMITMENT, VOTES));
        body.setSignatures(new HashMap<>());

        // build request
        Response response = TestSuite.target.path(RESOURCE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(body, MediaType.APPLICATION_JSON));

        // test response
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }
    */
}
