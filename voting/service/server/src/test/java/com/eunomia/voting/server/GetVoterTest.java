package com.eunomia.voting.server;

/**
 * Defines a batch of unit tests for the GET API Call <b>/voter</b>.
 */
public class GetVoterTest {

    // set API call path
    private static final String RESOURCE = "voting/voter";

}
