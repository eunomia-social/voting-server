/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.components;

import com.google.gson.Gson;

import java.io.Serializable;
import java.security.cert.X509Certificate;

/**
 * This abstract class is the parent class of the voting components (tallier,
 * manager and anonymizer). It is serializable so each of them can be saved
 * to a file and later recovered.
 */
public abstract class Component implements Serializable {

    /**
     * JSON Parser
     */
    protected static final Gson gson = new Gson();

    /**
     * X.509 Certificate for a voting component
     */
    private X509Certificate certificate;

    /**
     * Sets the certificate of the voting entity only if it has not been
     * set before.
     *
     * @param certificate X.509 certificate for the component
     */
    protected void setCertificate(X509Certificate certificate) {
        if (this.certificate != null) return;
        this.certificate = certificate;
    }

    /**
     * @return X.509 Certificate for the component ({@link #certificate})
     */
    protected X509Certificate getCertificate() {
        return certificate;
    }

}
