/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.server.filter;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Log the responses.
 * @author lmss
 */
@Provider
public class ResponseLoggingFilter implements ContainerResponseFilter {
    private static final Logger logger = LogManager.getLogger(ResponseLoggingFilter.class);

    @Override
    public void filter(ContainerRequestContext reqContext, ContainerResponseContext respContext) {
        Long ts = (Long)reqContext.getProperty("ts");
        if (ts != null) {
            long currentTimeStamp = System.currentTimeMillis();
            long elapsed = currentTimeStamp - ts;

            // output data
            logger.info("API: processing " + reqContext.getMethod() + " " + reqContext.getUriInfo().getRequestUri() + " took:" + elapsed + "ms.");
        }
    }
}
