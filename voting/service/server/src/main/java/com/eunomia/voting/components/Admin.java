/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.components;

import com.eunomia.voting.library.*;
import com.eunomia.voting.server.VotingServer;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.*;
import java.security.Security;
import java.util.Arrays;
import java.util.HashMap;

public class Admin {

    /**
     * This enum contains the ANSI color codes for some colors. It also produces
     * the colored output of a specific string, after initializing a certain color.
     */
    private enum Color {
        RED("\u001B[31m"),
        GREEN("\u001B[32m"),
        YELLOW("\u001B[33m");

        private final String code;
        Color(String code) {
            this.code = code;
        }

        public String write(String text) {
            return code + text + "\u001B[0m";
        }
    }

    /**
     * The Voting Server Universally Unique Identifier (UUID) distinguishes this voting
     * server from the others. It is received through the environment variable
     * VOTING_ID at the beginning of the program execution.
     */
    private static final String VOTING_ID = System.getenv(Constants.VOTING_ENV_VAR);

    /**
     * This token identifies the voting server towards the other components of
     * the EUNOMIA node and allows it to request EUNOMIA tokens. It is received
     * through the environment variable VOTING_TOKEN at the beginning of the
     * program execution.
     */
    // Yr0s7gYKLQvxof17VBsBlrNMNC_se8d4AOz4APCfKNI
    private static final String IDENTIFIER_TOKEN = System.getenv(Constants.TOKEN_ENV_VAR);

    /**
     * This variable identifier the underlying social networking platform of
     * this EUNOMIA node. It is received through the environment variable
     * SOCIAL_NETWORK at the beginning of the program execution.
     */
    private static final SocialNetworking.Platform SOCIAL_NETWORK = parsePlatform(System.getenv(Constants.SOCIAL_NETWORK_ENV_VAR));

    /**
     * The authentication provider is responsible for verifying an identifier
     * token and confirm that it is valid. This operation is carried out through
     * the EUNOMIA infrastructure. It is received through the environment variable
     * AUTH_PROVIDER at the beginning of the program execution.
     */
    private static final String AUTH_PROVIDER = System.getenv(Constants.AUTH_ENV_VAR);

    /**
     * The required number of talliers signatures sets threshold of signatures
     * to gather from other talliers of the federation when voting. It is received
     * through the environment variable REQUIRED_SIGNATURES at the beginning of the
     * program execution and then passed on to the tallier.
     */
    private static final Integer REQUIRED_SIGNATURES = tryParse(System.getenv(Constants.SIGNATURES_ENV_VAR));

    /**
     * Defines the absolute path for the voting server storage folder, which corresponds
     * to the relative path <b>./storage</b>.
     */
    private static final String STORAGE_DIRECTORY = System.getProperty("user.dir").concat("/storage/");

    /**
     * Voting Server EUNOMIA token to access the system's infrastructure (obtained
     * through the {@link #IDENTIFIER_TOKEN}).
     */
    private static String accessToken = null;

    //------------------------------------------------------------------------------------------
    // INITIALIZE SERVICE
    //------------------------------------------------------------------------------------------

    /**
     * Initializes the voting server.
     *
     * <p>The initialization procedure is as follows:
     * <ol>
     *     <li>Verify if the environment variables are properly set (VOTING_ID, VOTING_TOKEN, AUTH_PROVIDER)</li>
     *     <li>Look for previous state and restore it</li>
     *     <li>Generate credentials for Manager, Tallier and Anonymizer only if there was no
     *     previous state</li>
     *     <li>Save the credentials in the <b>./storage</b> directory</li>
     *     <li>Retrieve a new token from the EUNOMIA infrastructure</li>
     *     <li>Register with the communication service</li>
     *     <li>Post the X.509 certificates of the components to the discovery service</li>
     * </ol>
     *
     * @return Boolean to assess if the initialization process was successful
     */
    public static boolean initialize() {
        // add security providers
        Security.addProvider(new BouncyCastleProvider());

        // check environment variables
        System.out.println("Verifying Environment Variables...");

        // check voting id
        if (VOTING_ID == null) { System.out.println("... " + Constants.VOTING_ENV_VAR + Color.RED.write(" INVALID")); return false; }
        else System.out.println("... " + Constants.VOTING_ENV_VAR + Color.GREEN.write(" OK"));

        // check voting server identifier token
        if (IDENTIFIER_TOKEN == null) { System.out.println("... " + Constants.TOKEN_ENV_VAR + Color.RED.write(" INVALID")); return false; }
        else System.out.println("... " + Constants.TOKEN_ENV_VAR + Color.GREEN.write(" OK"));

        // check social network platform
        if (SOCIAL_NETWORK == null) {
            System.out.println("... " + Constants.SOCIAL_NETWORK_ENV_VAR + Color.RED.write(" INVALID"));
            System.out.println("Supported Social Networks: " + Arrays.asList(SocialNetworking.Platform.values()));
            return false;
        } else System.out.println("... " + Constants.SOCIAL_NETWORK_ENV_VAR + Color.GREEN.write(" OK"));

        // check auth provider
        if (AUTH_PROVIDER == null) { System.out.println("... " + Constants.AUTH_ENV_VAR + Color.RED.write(" INVALID")); return false; }
        else System.out.println("... " + Constants.AUTH_ENV_VAR + Color.GREEN.write(" OK"));

        // check required signatures
        if (REQUIRED_SIGNATURES < 1) { System.out.println("... " + Constants.SIGNATURES_ENV_VAR + Color.RED.write(" INVALID")); return false; }
        else System.out.println("... " + Constants.SIGNATURES_ENV_VAR + Color.GREEN.write(" OK"));

        // look for previous credentials and try to restore state
        System.out.printf("Restoring State for ID %s... ", VOTING_ID);
        boolean restore = restoreState();

        // check if restore was successful
        if (!restore) {
            // data not found
            System.out.println(Color.YELLOW.write("NO DATA FOUND"));

            // generate credentials for all entities
            System.out.println("Generating Credentials...");

            // generate keys for manager
            System.out.print("... Manager ");
            if (Manager.getInstance().getCertificate() == null) { System.out.println(Color.RED.write("FAILED")); return false; }
            System.out.println(Color.GREEN.write("OK"));

            // generate keys for tallier
            System.out.print("... Tallier ");
            if (Tallier.getInstance().getCertificate() == null) { System.out.println(Color.RED.write("FAILED")); return false; }
            System.out.println(Color.GREEN.write("OK"));

            // generate keys for anonymizer
            System.out.print("... Anonymizer ");
            if (Anonymizer.getInstance().getCertificate() == null) { System.out.println(Color.RED.write("FAILED")); return false; }
            System.out.println(Color.GREEN.write("OK"));

            // save credentials
            System.out.print("Saving Credentials... ");
            if (!saveState()) { System.out.println(Color.RED.write("FAILED")); return false; }
        }
        System.out.println(Color.GREEN.write("OK"));

        // retrieve eunomia token
        System.out.print("Retrieving EUNOMIA Token... ");
        if (!Admin.retrieveToken()) { System.out.println(Color.RED.write("FAILED")); return false; }
        System.out.println(Color.GREEN.write("OK"));

        // register with communication service
        System.out.print("Registering with Communication Service... ");
        if (!Communication.bind(accessToken, Constants.SERVICE_NAME, VOTING_ID, Constants.COMMUNICATION_CALLBACK)) {
            System.out.println(Color.RED.write("FAILED"));
            return false;
        }
        System.out.println(Color.GREEN.write("OK"));

        // build services hashmap
        HashMap<String, Object> services = printServices(true);

        // update information on discovery service
        System.out.print("Updating Discovery Service... ");
        boolean discovery;
        if (restore){
            discovery = Discovery.update(accessToken, services, VOTING_ID, Constants.SERVICE_NAME);
            if (!discovery) {
                System.out.println("Discovery update failed... Trying insert");
                discovery = Discovery.post(accessToken, services, VOTING_ID, Constants.SERVICE_NAME);
            }
        } else {
            discovery = Discovery.post(accessToken, services, VOTING_ID, Constants.SERVICE_NAME);
        }

        if (!discovery) { System.out.println(Color.RED.write("FAILED")); return false; }
        System.out.println(Color.GREEN.write("OK\n"));

        return true;
    }

    //------------------------------------------------------------------------------------------
    // SHUTDOWN SERVICE
    //------------------------------------------------------------------------------------------

    /**
     * Shuts down the voting server.
     * <p>This method is called when the running process is about to stop.
     *
     * <p>The shutdown procedure is as follows:
     * <ol>
     *     <li>Let all operations finish (ballot signing, anonymizing, ...)</li>
     *     <li>Unbind from communication service</li>
     *     <li>Update the server's state on discovery service</li>
     *     <li>Revoke the token associated with the voting server</li>
     * </ol>
     */
    public static void shutdown() {
        // shutdown server
        System.out.print("\nStopping server... ");
        VotingServer.stop();
        System.out.println(Color.GREEN.write("OK"));

        // unbind from communication service
        System.out.print("Unbinding from Communication Service... ");
        if (Communication.unbind(accessToken, VOTING_ID)) System.out.println(Color.GREEN.write("OK"));
        else System.out.println(Color.RED.write("FAILED"));

        // update credentials on discovery service
        System.out.print("Updating Discovery Service... ");
        HashMap<String, Object> services = printServices(false);
        if (Discovery.update(accessToken, services, VOTING_ID, Constants.SERVICE_NAME)) System.out.println(Color.GREEN.write("OK"));
        else System.out.println(Color.RED.write("FAILED"));

        // complete operations (dispatch all ballots)
        System.out.print("Completing Operations... ");
        Anonymizer.getInstance().dispatchAll();
        System.out.println(Color.GREEN.write("OK"));

        // revoke token
        System.out.print("Revoking EUNOMIA Token... ");
        if (Authentication.revokeToken(accessToken)) System.out.println(Color.GREEN.write("OK"));
        else System.out.println(Color.RED.write("FAILED"));
    }

    //------------------------------------------------------------------------------------------
    // HELPERS
    //------------------------------------------------------------------------------------------

    /**
     * Builds a hashmap containing the certificates of all services that
     * exist in a voting server.
     *
     * @param live flag to identify if these services are currently running
     * @return Hashmap with the services
     */
    private static HashMap<String, Object> printServices(boolean live) {
        HashMap<String, Object> services = new HashMap<>();
        services.put("manager", Cryptography.X509.printCertificate(Manager.getInstance().getCertificate()));
        services.put("tallier", Cryptography.X509.printCertificate(Tallier.getInstance().getCertificate()));
        services.put("anonymizer", Cryptography.X509.printCertificate(Anonymizer.getInstance().getCertificate()));
        services.put("live", live);

        return services;
    }

    /**
     * Tries to parse a string to an integer.
     *
     * @param str string to be parsed to integer
     * @return Parsed integer or zero (if parsing fails)
     */
    private static Integer tryParse(String str) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * Tries to parse a string to a social network platform.
     *
     * @param str string to be parsed to platform enum
     * @return Parsed value or null
     *
     * @see SocialNetworking.Platform
     */
    private static SocialNetworking.Platform parsePlatform(String str) {
        try {
            return SocialNetworking.Platform.valueOf(str.toUpperCase());
        } catch (IllegalArgumentException | NullPointerException e) {
            return null;
        }
    }

    //------------------------------------------------------------------------------------------
    // NODE PARAMETERS
    //------------------------------------------------------------------------------------------

    /**
     * Gets the Voting Server Universally Unique Identifier {@link #VOTING_ID}.
     *
     * @return Voting Server ID
     */
    public static String getVotingIdentifier() {
        return VOTING_ID;
    }

    /**
     * Gets EUNOMIA access token for the Voting Server ({@link #accessToken}).
     *
     * @return Access Token
     */
    public static String getAccessToken() {
        return accessToken;
    }

    /**
     * Gets the social networking identifier for this EUNOMIA
     * node ({@link #accessToken}).
     *
     * @return Social networking platform
     *
     * @see SocialNetworking.Platform
     */
    public static SocialNetworking.Platform getSocialNetwork() {
        return SOCIAL_NETWORK;
    }

    /**
     * Gets the social network authentication provider endpoint.
     *
     * @return Number of required signatures
     */
    public static String getAuthProvider() {
        return AUTH_PROVIDER;
    }

    /**
     * Gets the number of required tallier signatures for voting.
     *
     * @return Number of required signatures
     */
    public static Integer getRequiredSignatures() {
        return REQUIRED_SIGNATURES;
    }

    /**
     * Retrieves and saves a new EUNOMIA token for the Voting Server.
     * <p>Token is retrieved through the {@link Authentication#retrieveToken(String, String)}
     * method.
     *
     * @return Boolean to assess whether a new token was retrieved successfully
     */
    public static boolean retrieveToken() {
        accessToken = Authentication.retrieveToken(IDENTIFIER_TOKEN, AUTH_PROVIDER);
        return (accessToken != null);
    }

    //------------------------------------------------------------------------------------------
    // VOTING SERVER STATE (SAVE & RESTORE)
    //------------------------------------------------------------------------------------------

    /**
     * Saves the current state of the voting server to a file in the
     * <b>./storage</b> directory, with the filename being the identifier
     * of the voting server.
     *
     * @return Boolean to assess if the current state was saved
     */
    private static boolean saveState() {
        // check directory
        File storage = new File(STORAGE_DIRECTORY);
        if (!storage.mkdirs() && !storage.isDirectory())
            return false;

        // create state hashmap
        HashMap<String, Component> data = new HashMap<>();
        data.put("anonymizer", Anonymizer.getInstance());
        data.put("manager", Manager.getInstance());
        data.put("tallier", Tallier.getInstance());

        // write object
        try {
            // create output stream
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(STORAGE_DIRECTORY + VOTING_ID, false));

            // write object to file
            out.writeObject(data);

            // close output stream
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Restores the previous state of the voting server (only keys and certificates).
     *
     * @return Boolean to evaluate if restoration was successful
     */
    private static boolean restoreState() {
        // check if restore file exists
        File restore = new File(STORAGE_DIRECTORY + VOTING_ID);
        if (!restore.exists())
            return false;

        // write object
        try {
            // create input stream
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(STORAGE_DIRECTORY + VOTING_ID));

            // get object from file
            HashMap<String, Component> data = (HashMap<String, Component>)in.readObject();

            // restore components' state
            if (!Anonymizer.restore((Anonymizer)data.get("anonymizer")) ||
                !Manager.restore((Manager)data.get("manager")) ||
                !Tallier.restore((Tallier)data.get("tallier")))
                return false;

            // close input stream
            in.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
