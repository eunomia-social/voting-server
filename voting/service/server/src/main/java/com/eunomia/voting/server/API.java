/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.server;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Defines the voting server public API interface
 */
public interface API {
    Response createVoter(String token, String userId, String body);
    Response getVoter(String token, String userId);
    Response getBallot(String postId);
    Response getFeatures(String token, String userId, String body);
    Response hasVoted(String nym);
    Response deleteVote(String body);
    Response tally(String postId);
    Response tallies(List<String> postIds);
    Response vote(String body);
    Response anonymize(String token, String body);
    Response receive(String body);
}
