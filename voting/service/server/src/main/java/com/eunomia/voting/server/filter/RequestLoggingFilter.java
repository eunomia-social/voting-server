/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.server.filter;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Log the received requests.
 * @author lmss
 */
@Provider
public class RequestLoggingFilter implements ContainerRequestFilter{
    private static final Logger logger = LogManager.getLogger(ResponseLoggingFilter.class);

    @Override
    public void filter(ContainerRequestContext reqContext) {
        reqContext.setProperty("ts", System.currentTimeMillis());
        logger.info("API: Req: " + reqContext.getMethod() + " " + reqContext.getUriInfo().getRequestUri() + ".");
    }
}
