/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.components;

import kotlin.Pair;

import com.google.gson.reflect.TypeToken;
import com.google.gson.JsonSyntaxException;

import com.eunomia.voting.library.Communication;
import com.eunomia.voting.library.Authentication;
import com.eunomia.voting.library.Cryptography;
import com.eunomia.voting.library.RequestHelper;
import com.eunomia.voting.models.Ballot;
import com.eunomia.voting.models.Message;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.Response;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.concurrent.*;

public class Anonymizer extends Component {
    /**
     * Logger
     */
    private static final Logger logger = LogManager.getLogger(Anonymizer.class);

    /**
     * Class identifier for serialized data
     */
    private static final long serialVersionUID = -2157107855747144753L;

    /**
     * Time interval set for the scheduler to dispatch ballots
     */
    private static final Integer INTERVAL = 5;

    /**
     * Anonymizer instance (Singleton)
     */
    private static Anonymizer anonymizer = null;

    /**
     * Anonymizer's Private Key
     */
    private final PrivateKey privateKey;

    /**
     * Queue of messages to be delivered structured as
     * hashmap (key: receiver, value: message to be sent)
     * and concurrent to support multiple threads
     */
    private transient ConcurrentHashMap<String, Message> queue = new ConcurrentHashMap<>();

    /**
     * Periodically dispatches ballots from the queue
     */
    private transient ScheduledExecutorService dispatcher = Executors.newSingleThreadScheduledExecutor();

    /**
     * Private constructor to respect the singleton pattern.
     */
    private Anonymizer() {
        // generate credentials
        Pair<X509Certificate, PrivateKey> credentials = Cryptography.RSA.generateCredentials("Anonymizer_" + Admin.getVotingIdentifier());

        // assign credentials
        super.setCertificate(credentials.getFirst());
        this.privateKey = credentials.getSecond();

        // start executor service
        dispatcher.scheduleAtFixedRate(this::dispatchAll, 0, INTERVAL, TimeUnit.SECONDS);
    }

    /**
     * Generates a new anonymizer instance or returns the existing one.
     *
     * @return Saved instance of the Anonymizer ({@link #anonymizer})
     */
    public static Anonymizer getInstance() {
        if (anonymizer == null) {
            anonymizer = new Anonymizer();
        }
        return anonymizer;
    }

    /**
     * Restores the state of the anonymizer using a previously initialized
     * and saved instance only if it has not been set before.
     *
     * @param a anonymizer instance to be restored
     * @return Boolean to evaluate the success of the operation
     */
    public static boolean restore(Anonymizer a) {
        if (anonymizer == null) {
            anonymizer = a;
            anonymizer.queue = new ConcurrentHashMap<>();
            anonymizer.dispatcher = Executors.newSingleThreadScheduledExecutor();
            anonymizer.dispatcher.scheduleAtFixedRate(anonymizer::dispatchAll, 0, INTERVAL, TimeUnit.SECONDS);

            return true;
        }
        return false;
    }

    //------------------------------------------------------------------------------------------
    // ANONYMIZE DATA
    //------------------------------------------------------------------------------------------

    /**
     * Handles the initial phase of anonymization.
     *
     * @param token token to prove the user is logged in and valid
     * @param info information about the data being decrypted
     * @param data data to be anonymized
     * @return Response stating success or error
     *
     * @see #process(String, String)
     */
    public Response anonymize(String token, String info, String data) {
        // verify eunomia token
        if (!Authentication.verifyToken(token)) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.ANONYMIZATION, null,
                            "Access token is invalid"))
                    .build();
        }

        // remove encryption and process data
        if (process(info, data)) {
            return Response.ok()
                    .entity(RequestHelper.buildResponse("Ballot successfully set for anonymization", null, null))
                    .build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.ANONYMIZATION, null,
                            "Could not prepare ballot for anonymization"))
                    .build();
        }
    }

    /**
     * Removes the outer layer of encryption from the data received
     * and prepares the data to be sent to the next anonymizer or
     * to be processed by the tallier.
     *
     * @param info information to decrypt data (key, iv) and
     *             identifier of the next anonymizer
     * @param data data to be decrypted and forwarded
     * @return Boolean to assess if operation was successful
     */
    public boolean process(String info, String data) {
        logger.info("Received a new ballot for anonymization");

        // decrypt information and verify
        String infoDecrypted = Cryptography.RSA.decrypt(info, privateKey);
        if (infoDecrypted == null) return false;

        // convert to hashmap
        HashMap<String, String> infoMap;
        try {
            infoMap = gson.fromJson(infoDecrypted, new TypeToken<HashMap<String, String>>(){}.getType());
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return false;
        }

        // symmetric encryption parameters
        SecretKeySpec key = Cryptography.AES.readKey(infoMap.get("key"));
        IvParameterSpec iv = Cryptography.AES.readIV(infoMap.get("iv"));

        // decrypt outer layer
        String dataDecrypted = Cryptography.AES.decrypt(data, key, iv);
        if (dataDecrypted == null) return false;

        // get next anonymizer
        String next = infoMap.get("next");
        if (next == null) {
            // parse ballot and signatures
            Ballot ballot;
            try {
                HashMap<String, Object> ballotInfo = gson.fromJson(dataDecrypted, new TypeToken<HashMap<String, Object>>(){}.getType());
                ballot = gson.fromJson(gson.toJson(ballotInfo.get("ballot")), Ballot.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                return false;
            }

            // forward to tallier (ignore response)
            Tallier.getInstance().vote(ballot);

            return true;
        }

        // sign message
        String signature = Cryptography.RSA.sign(dataDecrypted, privateKey);

        // prepare and enqueue message
        Message message = new Message(Message.Operation.ANONYMIZE, dataDecrypted, signature);
        queue.put(next, message);

        return true;
    }

    /**
     * Dispatches all ballots available in the queue to the respective
     * following nodes. Should the queue be empty, the dispatcher does
     * nothing.
     */
    protected void dispatchAll() {
        // verify queue elements
        if (queue.isEmpty()) return;

        logger.info("Dispatching all ballots");

        // randomize order first?

        // dispatch all messages
        for (HashMap.Entry<String, Message> entry : queue.entrySet()) {
            // map receiver and send message
            String receiver = Communication.mapNode(Admin.getAccessToken(), entry.getKey());
            Communication.send(Admin.getAccessToken(), Admin.getVotingIdentifier(), new String[] {receiver}, entry.getValue());
        }

        // deletes all messages from queue
        queue.clear();
    }
}
