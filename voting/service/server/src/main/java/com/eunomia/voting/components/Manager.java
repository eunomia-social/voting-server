/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.components;

import kotlin.Pair;

import com.google.gson.JsonSyntaxException;

import com.eunomia.voting.library.*;
import com.eunomia.voting.models.Feature;
import com.eunomia.voting.models.Ballot;
import com.eunomia.voting.models.Proof;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.Response;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.*;

public class Manager extends Component {
    /**
     * Logger
     */
    private static final Logger logger = LogManager.getLogger(Manager.class);

    /**
     * Class identifier for serialized data
     */
    private static final long serialVersionUID = -7229872567307814708L;

    /**
     * Manager instance (Singleton)
     */
    private static Manager manager = null;

    /**
     * Manager's Private Key
     */
    private final PrivateKey privateKey;

    /**
     * Private constructor to respect the singleton pattern.
     */
    private Manager() {
        // generate credentials
        Pair<X509Certificate, PrivateKey> credentials = Cryptography.RSA.generateCredentials("Manager_" + Admin.getVotingIdentifier());

        // assign credentials
        super.setCertificate(credentials.getFirst());
        privateKey = credentials.getSecond();
    }

    /**
     * Generates a new Manager instance or returns the existing one.
     *
     * @return Saved instance of the Anonymizer ({@link #manager})
     */
    public static Manager getInstance() {
        if (manager == null) {
            manager = new Manager();
        }
        return manager;
    }

    /**
     * Restores the state of the manager using a previously initialized
     * and saved instance only if it has not been set before.
     *
     * @param m manager instance to be restored
     * @return Boolean to evaluate the success of the operation
     */
    public static boolean restore(Manager m) {
        if (manager == null) {
            manager = m;
            return true;
        }
        return false;
    }

    //------------------------------------------------------------------------------------------
    // VOTER (REGISTER & LOGIN)
    //------------------------------------------------------------------------------------------

    /**
     * Creates a new voter in the EUNOMIA system.
     *
     * @param token token to prove the user is logged in and valid
     * @param userId identifier of the EUNOMIA user
     * @param publicKey public key of the voter (base64 encoding)
     * @param privateKey encrypted private key (base64 encoding)
     * @return Response containing the status of the operation
     */
    public Response createVoter(String token, String userId, String publicKey, String privateKey) {
        logger.info(String.format("Starting voter registration for user %s", userId));

        // verify (and retrieve if necessary) eunomia token
        if (!Authentication.verifyToken(Admin.getAccessToken()) && !Admin.retrieveToken()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTER, null,
                            "Authentication service is unavailable"))
                    .build();
        }

        // verify user token
        if (!Authentication.verifyUser(token, userId)) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTER, null,
                            "Access token does not belong to this user or is invalid"))
                    .build();
        }

        // check if voter already exists
        List<RequestHelper.StoredObject> voters = Storage.get(Admin.getAccessToken(), new String[] {"type", "properties.user_id"},
                new RequestHelper.Operation[] {RequestHelper.Operation.EQ, RequestHelper.Operation.EQ}, new String[] {"voter", userId});
        if (voters == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTER, null,
                            "Storage service is unavailable"))
                    .build();
        } else if (voters.size() > 0) {
            return Response.status(Response.Status.CONFLICT)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTER, null,
                            "This user is already registered as a voter"))
                    .build();
        }

        // create objects to be saved
        HashMap<String, String> voter = new HashMap<>();
        voter.put("user_id", userId);
        voter.put("key", privateKey);

        HashMap<String, String> voterKey = new HashMap<>();
        voterKey.put("public_key", publicKey);

        // save keys on storage
        if (!Storage.save(Admin.getAccessToken(), voterKey, "voterKey") ||
                !Storage.save(Admin.getAccessToken(), voter, "voter")) {
            // TODO: rollback if one fails
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTER, null,
                            "Storage service is unavailable"))
                    .build();
        }

        // build response body
        HashMap<String, Object> body = new HashMap<>();
        body.put("user_id", userId);
        body.put("public_key", publicKey);

        // build response and calculate signature
        String response = RequestHelper.buildResponse("Voter successfully created", body, null);
        String responseSignature = Cryptography.RSA.sign(response, this.privateKey);

        // return signed response
        return Response.ok().entity(response).header("X-Signature", responseSignature).build();
    }

    /**
     * Extracts the encrypted private key of an existing voter through
     * the provided EUNOMIA user identifier.
     *
     * @param token token to prove the user is logged in and valid
     * @param userId identifier of the EUNOMIA user
     * @return Response containing the encrypted private key of the voter
     */
    public Response getVoter(String token, String userId) {
        // verify (and retrieve if necessary) eunomia token
        if (!Authentication.verifyToken(Admin.getAccessToken()) && !Admin.retrieveToken()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTER, null,
                            "Authentication service is unavailable"))
                    .build();
        }

        // verify user token
        if (!Authentication.verifyUser(token, userId)) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTER, null,
                            "Access token does not belong to this user or is invalid"))
                    .build();
        }

        // search for voter
        List<RequestHelper.StoredObject> voters = Storage.get(Admin.getAccessToken(), new String[] {"type", "properties.user_id"},
                new RequestHelper.Operation[] {RequestHelper.Operation.EQ, RequestHelper.Operation.EQ}, new String[] {"voter", userId});
        if (voters == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTER, null,
                            "Storage service is unavailable"))
                    .build();
        } else if (voters.size() == 0) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTER, null,
                            "This user is not registered as a voter"))
                    .build();
        }

        // parse voter info
        HashMap<String, Object> voter = voters.get(0).getProperties();
        if (!voter.containsKey("key")) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTER, null,
                            "Storage service returned invalid information"))
                    .build();
        }

        // build response body (with private key)
        HashMap<String, String> body = new HashMap<>();
        body.put("key", voter.get("key").toString());

        // build response and calculate signature
        String response = RequestHelper.buildResponse("Voter successfully returned", body, null);
        String responseSignature = Cryptography.RSA.sign(response, privateKey);

        // return signed response
        return Response.ok().entity(response).header("X-Signature", responseSignature).build();
    }

    //------------------------------------------------------------------------------------------
    // BALLOT
    //------------------------------------------------------------------------------------------

    /**
     * Builds a new ballot for a specific post.
     *
     * @param postId identifier of the post for which the ballot is being requested
     * @return Response containing the ballot or the error
     *
     * @see Manager#generateBallot(String)
     */
    public Response getBallot(String postId) {
        // verify postId and generate new ballot
        Ballot ballot = generateBallot(postId);
        if (ballot == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BALLOT, null,
                            String.format("Post %s does not exist", postId)))
                    .build();
        }

        // build response and calculate signature
        String response = RequestHelper.buildResponse("Ballot successfully generated", ballot, null);
        String responseSignature = Cryptography.RSA.sign(response, privateKey);

        // return ballot in a signed response
        return Response.ok().entity(response).header("X-Signature", responseSignature).build();
    }

    /**
     * Generates a ballot for a specific post.
     *
     * @param postId identifier of the post for which the ballot is being generated
     * @return Ballot object or null if the ballot could not be generated
     */
    private Ballot generateBallot(String postId) {
        // verify post
        if (!SocialNetworking.verifyPost(Admin.getSocialNetwork(), Admin.getAuthProvider(), postId))
            return null;

        // get characteristics for post from somewhere

        // build voting choices based on characteristics
        HashMap<String, Integer> choices = new HashMap<>();
        choices.put("trust", 0);
        choices.put("no_trust", 0);

        return new Ballot(postId, choices);
    }

    //------------------------------------------------------------------------------------------
    // FEATURES
    //------------------------------------------------------------------------------------------

    /**
     * Extracts a set of features for a specific user and returns them.
     *
     * @param token token to prove the user is logged in and valid
     * @param userId identifier of the object which stores data for the
     *               user on the social network
     * @param proof proof of knowledge for the key image
     * @return Response containing a list of features (and signatures) or the error
     *
     * @see SocialNetworking#getFeatures(SocialNetworking.Platform, String, String, String)
     * @see Manager#signFeatures(ArrayList, String)
     */
    public Response getFeatures(String token, String userId, Proof proof) {
        // verify user token
        if (!Authentication.verifyUser(token, userId)) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.FEATURES, null,
                            "The access token does not belong to this user or is invalid"))
                    .build();
        }

        // validate proof
        if (!Cryptography.ECC.verifyProof(proof, null, null)) {
            return Response.status(Response.Status.UNAUTHORIZED)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.FEATURES, null,
                            "The proof of knowledge for the key image is invalid"))
                    .build();
        }

        // get features from social network and sign
        ArrayList<Feature> features = SocialNetworking.getFeatures(Admin.getSocialNetwork(), Admin.getAuthProvider(), token, userId);
        ArrayList<Feature> signedFeatures = signFeatures(features, proof.getNym());

        // build response and calculate signature
        String response = RequestHelper.buildResponse("Features successfully listed", signedFeatures, null);
        String responseSignature = Cryptography.RSA.sign(response, privateKey);

        // return dictionary of features in a signed response
        return Response.ok().entity(response).header("X-Signature", responseSignature).build();
    }

    /**
     * Signs each feature of an array of features, along with
     * the key image of the user.
     *
     * @param features list of features to be signed
     * @param keyImage key image of the user (issued in the PoK)
     * @return list of features signed
     */
    private ArrayList<Feature> signFeatures(ArrayList<Feature> features, String keyImage) {
        // list of signed features (appended successively)
        ArrayList<Feature> signedFeatures = new ArrayList<>();

        // loop through features and sign
        for (Feature f : features) {
            String message = gson.toJson(f.getFeature()) + keyImage;
            String signature = Cryptography.RSA.sign(message, privateKey);

            f.setSignatures(Collections.singletonMap(Admin.getVotingIdentifier(), signature));
            signedFeatures.add(f);
        }

        return signedFeatures;
    }

    //------------------------------------------------------------------------------------------
    // ELIGIBILITY / PREVIOUS VOTE
    //------------------------------------------------------------------------------------------

    /**
     * Returns a response for a voter which contains the voting
     * state for a certain pseudonym. Additionally, obtains and
     * returns the ballot that was cast by the voter using this
     * pseudonym.
     *
     * @param nym pseudonym used by the voter
     * @return Response with voted boolean flag and a ballot
     * if exists
     */
    public Response hasVoted(String nym) {
        // verify (and retrieve if necessary) eunomia token
        if (!Authentication.verifyToken(Admin.getAccessToken()) && !Admin.retrieveToken()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.ELIGIBILITY, null,
                            "Authentication service is unavailable"))
                    .build();
        }

        // lookup the ballot
        List<RequestHelper.StoredObject> ballots = Storage.get(Admin.getAccessToken(), new String[] {"type", "properties.proof.nym"},
                new RequestHelper.Operation[] {RequestHelper.Operation.EQ, RequestHelper.Operation.EQ}, new String[] {"ballot", nym});
        if (ballots == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.ELIGIBILITY, null,
                            "Storage service is unavailable"))
                    .build();
        }

        // get ballot if exists
        Ballot ballot = null;
        if (ballots.size() > 0) {
            try {
                // get ballots
                ballot = gson.fromJson(gson.toJson(ballots.get(0).getProperties()), Ballot.class);
            } catch (JsonSyntaxException e) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.ELIGIBILITY, null,
                                "Storage service returned invalid information"))
                        .build();
            }

            // nullify useless fields
            ballot.setSignatures(null);
            ballot.setProof(null);
        }

        // build response body
        HashMap<String, Object> body = new HashMap<>();
        body.put("voted", ballot != null);
        if (ballot != null) body.put("ballot", ballot);

        // build response and calculate signature
        String response = RequestHelper.buildResponse("Previous voting successfully returned", body, null);
        String responseSignature = Cryptography.RSA.sign(response, privateKey);

        // return signed response
        return Response.ok().entity(response).header("X-Signature", responseSignature).build();
    }

    //------------------------------------------------------------------------------------------
    // DELETE VOTE
    //------------------------------------------------------------------------------------------

    /**
     * Deletes a previously cast ballot using a pseudonym.
     *
     * @param proof proof to ensure that only the owner of
     *              the ballot can delete the ballot
     * @param postId identifier of the post
     * @return Response with a boolean which states if the
     * ballot was deleted
     */
    public Response deleteVote(Proof proof, String postId) {
        // verify (and retrieve if necessary) eunomia token
        if (!Authentication.verifyToken(Admin.getAccessToken()) && !Admin.retrieveToken()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.DELETION, null,
                            "Authentication service is unavailable"))
                    .build();
        }

        // verify keys and proof
        if (!Tallier.getInstance().verifyKeys(proof.getKeys()) || !Cryptography.ECC.verifyProof(proof, postId, null)) {
            return Response.status(Response.Status.FORBIDDEN)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.DELETION, null,
                            "Deletion proof is invalid"))
                    .build();
        }

        // TODO: ring signature challenge should be kept to avoid replay attack

        // lookup the ballot
        List<RequestHelper.StoredObject> ballots = Storage.get(Admin.getAccessToken(), new String[] {"type", "properties.proof.nym"},
                new RequestHelper.Operation[] {RequestHelper.Operation.EQ, RequestHelper.Operation.EQ}, new String[] {"ballot", proof.getNym()});
        if (ballots == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.DELETION, null,
                            "Storage service is unavailable"))
                    .build();
        } else if (ballots.isEmpty()) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.DELETION, null,
                            "No ballots available for this pseudonym"))
                    .build();
        }

        // delete ballots
        ballots.forEach(ballot -> Storage.delete(Admin.getAccessToken(), ballot.getId()));

        // build response body
        HashMap<String, Object> body = new HashMap<>();
        body.put("deleted", true);

        // build response and calculate signature
        String response = RequestHelper.buildResponse("Ballot successfully deleted", body, null);
        String responseSignature = Cryptography.RSA.sign(response, privateKey);

        // return signed response
        return Response.ok().entity(response).header("X-Signature", responseSignature).build();
    }
}
