/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.server;

import com.eunomia.voting.components.Admin;
import com.eunomia.voting.library.Constants;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * This class runs the Voting Server web service.
 */
public class VotingServer {
    /**
     * Logger
     */
    private static Logger logger = LogManager.getLogger(VotingServer.class);

    /**
     * Web server running the voting service
     */
    private static HttpServer server;

    /**
     * Starts grizzly HTTP server exposing JAX-RS resources defined in
     * the <b>com.eunomia.voting.server</b> package of the application.
     *
     * @param uri address to start the service on
     * @return HttpServer that runs the API
     */
    public static HttpServer start(URI uri) {
        // add custom configuration properties to jersey and scan
        // for JAX-RS resources and providers in the referred package
        final ResourceConfig rc = new ResourceConfig();
        rc.property(ServerProperties.WADL_FEATURE_DISABLE, true);
        rc.packages("com.eunomia.voting.server");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at the specified uri
        return GrizzlyHttpServerFactory.createHttpServer(uri, rc);
    }

    /**
     * Stops the currently running server.
     */
    public static void stop() {
        // shutdown if started
        if (server.isStarted())
            server.shutdownNow();
    }

    public static void main(String[] args) {
        logger.info("Voting server starting...");

        // initialize voting service
        if (!Admin.initialize()) {
            logger.info("Shutting down...");
            System.exit(1);
        }

        // build uri and start server
        URI uri = UriBuilder.fromUri(Constants.API_BASE_URI).port(Constants.PORT_NUMBER).path(Constants.API_PATH).build();
        server = start(uri);

        // create shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(Admin::shutdown));

        // output status (if ok)
        logger.info(String.format("API is running on %s", uri));
        logger.info("CTRL-C to shutdown.");
    }
}
