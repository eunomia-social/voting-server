/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.server;

import com.eunomia.voting.components.Admin;
import com.eunomia.voting.components.Manager;
import com.eunomia.voting.components.Tallier;
import com.eunomia.voting.components.Anonymizer;
import com.eunomia.voting.library.Communication;
import com.eunomia.voting.library.Constants;
import com.eunomia.voting.library.Cryptography;
import com.eunomia.voting.library.RequestHelper;
import com.eunomia.voting.models.Ballot;
import com.eunomia.voting.models.Message;

import com.eunomia.voting.models.Proof;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.HashMap;
import java.util.List;

/**
 * This class implements all the API calls for the voting service as
 * defined by the voting protocol architecture.
 */
@Path("voting")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VotingService implements API {

    /**
     * JSON Parser
     */
    private static final Gson gson = new Gson();

    /**
     * POST API Call for path <b>/voter</b>.
     *
     * <p>Creates a new voter in the EUNOMIA system.
     *
     * <p>This call is handled by the Manager, specifically by the
     * {@link Manager#createVoter(String, String, String, String)} method.
     */
    @Override
    @Path("voter")
    @POST
    public Response createVoter(@QueryParam("access_token") String token, @QueryParam("user_id") String userId, String body) {
        // verify query parameters
        if (token == null || token.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Access token is missing"))
                    .build();
        } else if (userId == null || userId.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "User identifier is missing"))
                    .build();
        }

        // parse body
        HashMap<String, String> data;
        try {
            data = gson.fromJson(body, new TypeToken<HashMap<String, String>>(){}.getType());
        } catch (JsonSyntaxException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Body has invalid format"))
                    .build();
        }

        // check body format
        if (!data.containsKey("public_key") || !data.containsKey("private_key")) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Request body does not have the required keys"))
                    .build();
        }

        // set keypair (public + encrypted private)
        String pub = data.get("public_key");
        String priv = data.get("private_key");

        // read public key and check format
        if (!Cryptography.ECC.validatePoint(pub)) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Public key is invalid"))
                    .build();
        }

        // check encrypted private key format (key:iv:salt, base64)
        if (!priv.matches(Constants.BASE64_REGEX + "(:" + Constants.BASE64_REGEX + "){2}")) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Encrypted private key has invalid format"))
                    .build();
        }

        return Manager.getInstance().createVoter(token, userId, pub, priv);
    }

    /**
     * GET API Call for path <b>/voter</b>.
     *
     * <p>Returns the encrypted private key of an existing voter in the
     * EUNOMIA system.
     *
     * <p>This call is handled by the Manager, specifically by the
     * {@link Manager#getVoter(String, String)} method.
     */
    @Override
    @Path("voter")
    @GET
    public Response getVoter(@QueryParam("access_token") String token, @QueryParam("user_id") String userId) {
        // verify query parameters
        if (token == null || token.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Access token is missing"))
                    .build();
        } else if (userId == null || userId.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "User identifier is missing"))
                    .build();
        }

        return Manager.getInstance().getVoter(token, userId);
    }

    /**
     * GET API Call for path <b>/ballot</b>.
     *
     * <p>Builds a new ballot for a specific post and returns it.
     *
     * <p>This call is handled by the Manager, specifically by the
     * {@link Manager#getBallot(String)} method.
     */
    @Override
    @Path("ballot")
    @GET
    public Response getBallot(@QueryParam("post_id") String postId) {
        // verify request parameters
        if (postId == null || postId.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Post identifier is missing or invalid"))
                    .build();
        }

        return Manager.getInstance().getBallot(postId);
    }

    /**
     * GET API Call for path <b>/features</b>.
     *
     * <p>Extracts a set of features for a specific user and returns them.
     *
     * <p>This call is handled by the Manager, specifically by the
     * {@link Manager#getFeatures(String, String, Proof)}
     */
    @Override
    @Path("features")
    @POST
    public Response getFeatures(@QueryParam("access_token") String token, @QueryParam("user_id") String userId, String body) {
        // verify request parameters
        if (token == null || token.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Access token is missing"))
                    .build();
        } else if (userId == null || userId.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "User identifier is missing"))
                    .build();
        }

        // parse key image proof
        Proof proof;
        try {
            proof = gson.fromJson(body, Proof.class);
        } catch (JsonSyntaxException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Key image proof has invalid format"))
                    .build();
        }

        // check proof parameters
        if (proof == null || !proof.hasValidFormat()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Key image proof does exist or has invalid format"))
                    .build();
        }

        return Manager.getInstance().getFeatures(token, userId, proof);
    }

    /**
     * GET API Call for path <b>/eligibility</b>.
     *
     * <p>Verifies if the voter has voted before on a post through the
     * provided pseudonym. Returns the ballot if true.
     *
     * <p>This call is handled by the Manager, specifically by the
     * {@link Manager#hasVoted(String)} method.
     */
    @Override
    @Path("eligibility")
    @GET
    public Response hasVoted(@QueryParam("nym") String nym) {
        // check nym validity
        if (nym == null || nym.isBlank() || !nym.matches(Constants.BASE64_REGEX)) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Pseudonym is missing or has invalid format"))
                    .build();
        }

        return Manager.getInstance().hasVoted(nym);
    }

    /**
     * POST API Call for path <b>/unvote</b>.
     *
     * <p>The manager deletes a previously recorded ballot through the
     * provided pseudonym.
     *
     * <p>This call is handled by the Manager, specifically by the
     * {@link Manager#deleteVote(Proof, String)} method.
     */
    @Override
    @Path("unvote")
    @POST
    public Response deleteVote(String body) {
        // verify request parameters
        if (body == null || body.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Request body does not exist"))
                    .build();
        }

        // parse body
        Ballot ballot;
        try {
            ballot = gson.fromJson(body, Ballot.class);
        } catch (JsonSyntaxException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Body has invalid format"))
                    .build();
        }

        // verify format
        if (ballot == null || ballot.getPostId() == null || ballot.getProof() == null || !ballot.getProof().hasValidFormat()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Request body does not have the required parameters or has invalid parameters"))
                    .build();
        }

        return Manager.getInstance().deleteVote(ballot.getProof(), ballot.getPostId());
    }

    /**
     * GET API Call for path <b>/tally</b>.
     *
     * <p>Gets the voting results for a specific post.
     *
     * <p>This call is handled by the Tallier, specifically by the
     * {@link Tallier#tally(String)} method.
     */
    @Override
    @Path("tally")
    @GET
    public Response tally(@QueryParam("post_id") String postId) {
        // verify request parameters
        if (postId == null || postId.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Post identifier is missing or invalid"))
                    .build();
        }

        return Tallier.getInstance().tally(postId);
    }

    /**
     * GET API Call for path <b>/tallies</b>
     *
     * <p> Get the voting results for a specified list of posts. </p>
     *
     * <p>This call is handled by the Tallier, specifically by the
     * {@link Tallier#tallies(List)} method.
     */
    @Override
    @Path("tallies")
    @GET
    public Response tallies(@QueryParam("post_id") List<String> postIds) {
        // verify request parameters
        if (postIds == null || postIds.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Post identifiers are missing or invalid"))
                    .build();
        }

        return Tallier.getInstance().tallies(postIds);
    }

    /**
     * POST API Call for path <b>/vote</b>.
     *
     * <p>Adds a ballot to the post tally.
     *
     * <p>This call is handled by the Tallier, specifically by the
     * {@link Tallier#vote(Ballot)} method.
     */
    @Override
    @Path("vote")
    @POST
    public Response vote(String body) {
        // verify request parameters
        if (body == null || body.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Request body does not exist"))
                    .build();
        }

        // parse body
        Ballot ballot;
        try {
            ballot = gson.fromJson(body, Ballot.class);
        } catch (JsonSyntaxException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Ballot has invalid format"))
                    .build();
        }

        // verify ballot format
        if (ballot == null || ballot.getPostId() == null || ballot.getVotes() == null || ballot.getProof() == null
                || !ballot.getProof().hasValidFormat() || ballot.getSignatures() != null) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Ballot does not have the required parameters or has invalid parameters"))
                    .build();
        }

        // verify features format (if exist)
        if (ballot.getFeatures() != null && ballot.getFeatureProof() != null && !ballot.getFeatureProof().hasValidFormat()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Features do not have the required parameters or have invalid parameters"))
                    .build();
        }

        return Tallier.getInstance().vote(ballot);
    }

    /**
     * POST API Call for path <b>/anonymize</b>.
     *
     * <p>This is the entry point for the anonymization circuit created by
     * the client. The anonymizer mixes the new ballot with ones received
     * before and delays them before forwarding them to the next anonymizer.
     *
     * <p>This call is handled by the Anonymizer.
     */
    @Override
    @Path("anonymize")
    @POST
    public Response anonymize(@QueryParam("access_token") String token, String body) {
        // verify request parameters
        if (token == null || token.isBlank()) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Access token is missing"))
                    .build();
        }

        // parse body
        HashMap<String, String> data;
        try {
            data = gson.fromJson(body, new TypeToken<HashMap<String, String>>(){}.getType());
        } catch (JsonSyntaxException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Body has invalid format"))
                    .build();
        }

        // verify if body contains the required keys
        if (!data.containsKey("info") || !data.containsKey("data")) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.BAD_REQUEST, null,
                            "Body does not contain the required keys"))
                    .build();
        }

        return Anonymizer.getInstance().anonymize(token, data.get("info"), data.get("data"));
    }

    /**
     * POST API Call for path <b>/receive</b>.
     *
     * <p>This endpoint is used to receive message from other nodes.
     * It is associated with this voting server on the communication service.
     *
     * <p>This call is handled locally.
     */
    @Override
    @Path("receive")
    @POST
    public Response receive(String body) {
        // parse body
        Message message;
        String sender;
        try {
            // convert body to hashmap and get sender
            HashMap<String, String> data = gson.fromJson(body, new TypeToken<HashMap<String, Object>>(){}.getType());
            sender = data.get("sender");

            // parse message
            message = gson.fromJson(data.get("content"), Message.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            return Response.ok().build();
        }

        System.out.printf("Received a message from %s\n", sender);

        // check message parameters
        if (message.getOperation() == null || message.getData() == null /* || message.getSignature() == null*/)
            return Response.ok().build();

        // TODO: check message signature

        // parse operation
        switch (message.getOperation()) {
            case ANONYMIZE:
                try {
                    // parse message content and process message
                    HashMap<String, String> data = gson.fromJson(gson.toJson(message.getData()), new TypeToken<HashMap<String, String>>(){}.getType());
                    Anonymizer.getInstance().process(data.get("info"), data.get("data"));
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    return Response.ok().build();
                }
                break;
            case VERIFY_BALLOT:
                System.out.println("Ballot to verify...");
                // verify ballot received
                Ballot ballot;
                try {
                    ballot = gson.fromJson(gson.toJson(message.getData()), Ballot.class);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    return Response.ok().build();
                }

                // verify parameters
                if (ballot.getPostId() == null || ballot.getVotes() == null || ballot.getProof() == null)
                    return Response.ok().build();

                // sign ballot
                String ballotSignature = Tallier.getInstance().signBallot(ballot);
                if (ballotSignature != null && !ballotSignature.isEmpty()) {
                    // build message data
                    HashMap<String, String> data = new HashMap<>();
                    data.put("nym", ballot.getProof().getNym());
                    data.put("signature", ballotSignature);

                    // build message
                    String nodeId = Communication.mapNode(Admin.getAccessToken(), sender);
                    Message response = new Message(Message.Operation.TALLIER_AWAIT, data);

                    // send message
                    Communication.send(Admin.getAccessToken(), Admin.getVotingIdentifier(), new String[] {nodeId}, response);
                }
                break;
            case TALLIER_AWAIT:
                try {
                    // parse message content and call tallier
                    HashMap<String, String> data = gson.fromJson(gson.toJson(message.getData()),
                            new TypeToken<HashMap<String, String>>(){}.getType());
                    if (data.containsKey("nym") && data.containsKey("signature"))
                        Tallier.getInstance().receiveSignature(data.get("nym"), sender, data.get("signature"));
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    return Response.ok().build();
                }
                break;
            default:
                return Response.ok().build();
        }

        // return ok
        return Response.ok().build();
    }
}
