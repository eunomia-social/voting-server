/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.components;

import kotlin.Pair;

import com.google.gson.JsonSyntaxException;

import com.eunomia.voting.library.*;
import com.eunomia.voting.models.Ballot;
import com.eunomia.voting.models.Message;
import com.eunomia.voting.models.Proof;
import com.eunomia.voting.models.Feature;

import javax.ws.rs.core.Response;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Tallier extends Component {
    /**
     * Logger
     */
    private static final Logger logger = LogManager.getLogger(Tallier.class);

    /**
     * Class identifier for serialized data
     */
    private static final long serialVersionUID = -3801929132273920426L;

    /**
     * Timeout to get all required signatures for a ballot
     * to be valid (in seconds)
     */
    private static final Integer TIMEOUT = 5;

    /**
     * Number of required signatures from all talliers (including
     * this one) in order to validate a ballot
     */
    private static final Integer REQUIRED_SIGNATURES = Admin.getRequiredSignatures();

    /**
     * Tallier instance (Singleton)
     */
    private static Tallier tallier = null;

    /**
     * Tallier's Private Key
     */
    private final PrivateKey privateKey;

    /**
     * Map of ballots that have signature requests pending
     */
    private transient ConcurrentHashMap<String, HashMap<String, String>> pendingBallots = new ConcurrentHashMap<>();

    /**
     * Cache of eligible voters' public keys
     */
    private transient CopyOnWriteArrayList<String> voterCache = new CopyOnWriteArrayList<>();

    /**
     * Cache that maps other talliers to their X.509 certificates (it
     * is transient so it is not saved on shutdown)
     */
    private transient ConcurrentHashMap<String, X509Certificate> tallierCache = new ConcurrentHashMap<>();

    /**
     * Cache that maps managers to their X.509 certificates (it
     * is transient so it is not saved on shutdown)
     */
    private transient ConcurrentHashMap<String, X509Certificate> managerCache = new ConcurrentHashMap<>();

    /**
     * Private constructor to respect the singleton pattern.
     */
    private Tallier() {
        // generate credentials
        Pair<X509Certificate, PrivateKey> credentials = Cryptography.RSA.generateCredentials("Tallier_" + Admin.getVotingIdentifier());

        // assign credentials
        super.setCertificate(credentials.getFirst());
        privateKey = credentials.getSecond();

        // add certificate to cache
        tallierCache.put(Admin.getVotingIdentifier(), getCertificate());
    }

    /**
     * Generates a new Tallier instance or returns the existing one.
     *
     * @return Saved instance of the Anonymizer ({@link #tallier})
     */
    public static Tallier getInstance() {
        if (tallier == null) {
            tallier = new Tallier();
        }
        return tallier;
    }

    /**
     * Restores the state of the tallier using a previously initialized
     * and saved instance only if it has not been set before. Also,
     * restarts the necessary transient variables.
     *
     * @param t tallier instance to be restored
     * @return Boolean to evaluate the success of the operation
     */
    public static boolean restore(Tallier t) {
        if (tallier == null) {
            tallier = t;
            tallier.pendingBallots = new ConcurrentHashMap<>();
            tallier.voterCache = new CopyOnWriteArrayList<>();
            tallier.tallierCache = new ConcurrentHashMap<>();
            tallier.managerCache = new ConcurrentHashMap<>();
            tallier.tallierCache.put(Admin.getVotingIdentifier(), tallier.getCertificate());
            tallier.managerCache.put(Admin.getVotingIdentifier(), Manager.getInstance().getCertificate());

            return true;
        }
        return false;
    }

    //------------------------------------------------------------------------------------------
    // VOTE
    //------------------------------------------------------------------------------------------

    /**
     * Receives a ballot and optional features (inside), confirms the signatures and the
     * wellformedness and uniqueness of the ballot. If these conditions verify, the tallier
     * signs the ballot (see {@link Tallier#signBallot(Ballot)} and adds it to the
     * voting results. This request can come from an anonymizer or directly from a voter.
     *
     * @param ballot ballot to be added to the tally
     * @return Response with the code stating the state of the operation
     */
    public Response vote(Ballot ballot) {
        long start = System.nanoTime();
        logger.info(String.format("Started voting for post %s", ballot.getPostId()));

        // verify if post exists
        if (!SocialNetworking.verifyPost(Admin.getSocialNetwork(), Admin.getAuthProvider(), ballot.getPostId())) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTING, null,
                            String.format("Post %s does not exist", ballot.getPostId())))
                    .build();
        }

        //////// SIGN BALLOT ////////

        // ballot is only signed if valid and unique
        // features are checked here
        String signature = signBallot(ballot);
        if (signature == null) {
            return Response.status(Response.Status.FORBIDDEN)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTING, null,
                            "Ballot is invalid or user has voted before"))
                    .build();
        } else if (signature.isEmpty()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTING, null,
                            "Authentication service is unavailable"))
                    .build();
        }

        // get pseudonym
        String nym = ballot.getProof().getNym();

        // create map of signatures and save
        HashMap<String, String> signatures = new HashMap<>();
        signatures.put(Admin.getVotingIdentifier(), signature);
        pendingBallots.put(nym, signatures);

        ///// GATHER SIGNATURES /////

        // broadcast to other talliers
        // set REQUIRED_SIGNATURES = 1 to ignore this step
        logger.info(String.format("To request other signatures from %d talliers...", (REQUIRED_SIGNATURES - 1)));
        Message message = new Message(Message.Operation.VERIFY_BALLOT, ballot);
        if (!Communication.broadcast(Admin.getAccessToken(), Admin.getVotingIdentifier(), REQUIRED_SIGNATURES - 1, message)) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTING, null,
                            "Could not communicate with other talliers"))
                    .build();
        }

        // check if broadcast happened
        if (REQUIRED_SIGNATURES > 1) {
            // grizzly thread now owns the object
            synchronized (pendingBallots.get(nym)) {
                // wait for signatures
                try {
                    logger.info("Waiting for signatures...");
                    pendingBallots.get(nym).wait(TIMEOUT * 1000);
                } catch (Exception e) {
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                            .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTING, null,
                                    "Could not communicate with other talliers"))
                            .build();
                }
            }
        }

        // verify if the required signatures were received
        if (pendingBallots.get(nym).size() < REQUIRED_SIGNATURES) {
            pendingBallots.remove(nym);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTING, null,
                            "Could not communicate with other talliers"))
                    .build();
        }

        // append signatures to ballot
        ballot.setSignatures(pendingBallots.get(nym));

        //////// POST BALLOT ////////

        // remove key image proof and feature
        // signatures before posting
        ballot.setFeatureProof(null);
        if (ballot.getFeatures() != null)
            ballot.getFeatures().forEach(f -> f.setSignatures(null));

        // add to storage
        if (!Storage.save(Admin.getAccessToken(), ballot, "ballot")) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.VOTING, null,
                            "Ballot could not be saved"))
                    .build();
        }

        // delete nym locally only after uploading to storage
        // otherwise there would be a window of attack
        pendingBallots.remove(nym);

        // prepare ballot for response
        ballot.setProof(null);
        ballot.setSignatures(null);

        // build response and signature
        String response = RequestHelper.buildResponse("Ballot successfully set for counting", ballot, null);
        String responseSignature = Cryptography.RSA.sign(response, privateKey);

        long end = System.nanoTime();
        logger.info(String.format("Took %d milliseconds to vote on postId %s",
                ((end - start) / 1000000), ballot.getPostId()));

        // return signed response
        return Response.ok().entity(response).header("X-Signature", responseSignature).build();
    }

    /**
     * Signs the ballot (which includes postId, votes and features) if the
     * following conditions are fulfilled.
     *
     * <ul>
     *     <li>Check the tags of the <i>votes</i> key (well-formed)</li>
     *     <li>Verify the key image proof and the signatures on the features</li>
     *     <li>Verify the ring signature</li>
     *     <li>Assess the uniqueness of the ballot using the pseudonym</li>
     * </ul>
     *
     * @param ballot ballot to be verified
     * @return Signature of the ballot if the conditions are fulfilled, null otherwise
     */
    public String signBallot(Ballot ballot) {
        // verify (and retrieve if necessary) eunomia token for voting server
        if (!Authentication.verifyToken(Admin.getAccessToken()) && !Admin.retrieveToken())
            return "";

        // TODO: check if votes are well-formed
        HashMap<String, Integer> votes = ballot.getVotes();

        // get proofs
        Proof proof = ballot.getProof();
        Proof featureProof = ballot.getFeatureProof();

        Ballot signedBallot = new Ballot(ballot.getPostId(), votes);

        // validate features
        if (ballot.getFeatures() != null && featureProof != null) {
            // verify key image proof and feature signatures
            if (!verifyKeys(featureProof.getKeys()) || !Cryptography.ECC.verifyProof(featureProof, null, null)) {
                return null;
            } else if (!validateFeatures(ballot.getFeatures(), featureProof.getNym())) {
                return null;
            }

            // update signed ballot and remove feature signatures
            signedBallot.setFeatures(ballot.getFeatures());
            signedBallot.getFeatures().forEach(f -> f.setSignatures(null));
        }

        // verify ring signature
        if (!verifyKeys(proof.getKeys()) || !Cryptography.ECC.verifyProof(proof, ballot.getPostId(), gson.toJson(signedBallot)))
            return null;

        // confirm uniqueness (do not sign if response is invalid or nym exists locally or remotely)
        List<RequestHelper.StoredObject> objects = Storage.get(Admin.getAccessToken(), new String[] {"type", "properties.proof.nym"},
                new RequestHelper.Operation[] {RequestHelper.Operation.EQ, RequestHelper.Operation.EQ},
                new String[] {"ballot", proof.getNym()});
        if (objects == null || objects.size() != 0 || pendingBallots.containsKey(proof.getNym()))
            return null;

        // sign json ballot
        return Cryptography.RSA.sign(gson.toJson(signedBallot), privateKey);
    }

    /**
     * Confirms that the keys received belongs to registered voters in the
     * EUNOMIA system.
     *
     * @param keys array of keys to be verified
     * @return Boolean to assess if all keys belong to voters
     */
    public boolean verifyKeys(String[] keys) {
        // update list if necessary
        if (!voterCache.containsAll(Arrays.asList(keys))) {
            List<RequestHelper.StoredObject> objects = Storage.get(Admin.getAccessToken(), new String[] {"type"},
                    new RequestHelper.Operation[] {RequestHelper.Operation.EQ}, new String[] {"voterKey"});
            if (objects == null || voterCache.size() >= objects.size())
                return false;

            // update list
            objects.forEach(k -> voterCache.addIfAbsent(k.getProperties().get("public_key").toString()));
        }

        return voterCache.containsAll(Arrays.asList(keys));
    }

    /**
     * Handles a new signature received from a request of this tallier.
     * This signature is added to the queued ballots.
     *
     * @param nym pseudonym used to identify the ballot
     * @param signer voting server identifier
     * @param signature ballot signature
     */
    public void receiveSignature(String nym, String signer, String signature) {
        if (pendingBallots.containsKey(nym)) {
            pendingBallots.get(nym).putIfAbsent(signer, signature);
            if (pendingBallots.get(nym).entrySet().size() >= REQUIRED_SIGNATURES) {
                synchronized (pendingBallots.get(nym)) {
                    pendingBallots.get(nym).notify();
                }
            }
        }
    }

    //------------------------------------------------------------------------------------------
    // TALLY
    //------------------------------------------------------------------------------------------

    /**
     * Produces a tally for a specific post.
     *
     * @param postId identifier of the post for which the tally is being calculated
     * @return Response object with the results of the tally
     */
    public Response tally(String postId) {
        logger.info(String.format("Started tallying for post %s", postId));

        // verify (and retrieve if necessary) eunomia token
        if (!Authentication.verifyToken(Admin.getAccessToken()) && !Admin.retrieveToken()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.TALLY, null,
                            "Authentication service is unavailable"))
                    .build();
        }

        // verify if post exists
        if (!SocialNetworking.verifyPost(Admin.getSocialNetwork(), Admin.getAuthProvider(), postId)) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.TALLY, null,
                            String.format("Post %s does not exist", postId)))
                    .build();
        }

        long start = System.nanoTime();

        // get ballots for post
        List<Ballot> ballots = getBallotsForPost(postId);
        if (ballots == null) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.TALLY, null,
                            "Storage service is unavailable"))
                    .build();
        }

        // build response
        HashMap<String, Object> data = new HashMap<>();
        data.put("post_id", postId);
        data.put("results", ballots);

        // build response and calculate signature
        String response = RequestHelper.buildResponse("Post tally successfully generated", data, null);
        String responseSignature = Cryptography.RSA.sign(response, privateKey);

        long end = System.nanoTime();
        logger.info(String.format("Took %d milliseconds to issue the tally for post %s", (end - start) / 1000000, postId));

        // return post tally result with a signed response
        return Response.ok().entity(response).header("X-Signature", responseSignature).build();
    }

    /**
     * Produces tallies for multiple posts.
     *
     * @param postIds list with post identifiers
     * @return Response object with the tallies
     */
    public Response tallies(List<String> postIds){
        logger.info(String.format("Started multiple tallies for posts %s", postIds));

        // verify (and retrieve if necessary) eunomia token
        if (!Authentication.verifyToken(Admin.getAccessToken()) && !Admin.retrieveToken()) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.TALLY, null,
                            "Authentication service is unavailable"))
                    .build();
        }

        HashMap<String, Object> hash = new HashMap<>();
        for (String postId : postIds) {
            // verify if post exists
            if (!SocialNetworking.verifyPost(Admin.getSocialNetwork(), Admin.getAuthProvider(), postId)) {
                return Response.status(Response.Status.NOT_FOUND)
                        .entity(RequestHelper.buildResponse(RequestHelper.ErrorMessage.TALLY, null,
                                String.format("Post %s does not exist", postId)))
                        .build();
            }

            List<Ballot> ballots = getBallotsForPost(postId);
            if (ballots == null) {
                // ignored
                logger.warn(String.format("No ballots for post %s", postId));
                continue;
            }

            // build response
            HashMap<String, Object> data = new HashMap<>();
            //data.put("post_id", postId);
            data.put("results", ballots);
            hash.put(postId, data);
        }

        // build response and calculate signature
        String response = RequestHelper.buildResponse("Post tally successfully generated", hash, null);
        String responseSignature = Cryptography.RSA.sign(response, privateKey);

        // return post tally result with a signed response
        return Response.ok().entity(response).header("X-Signature", responseSignature).build();
    }

    //------------------------------------------------------------------------------------------
    // HELPERS
    //------------------------------------------------------------------------------------------

    /**
     * Extracts all the ballots from storage for a specific post.
     *
     * @param postId identifier of the post
     * @return List of ballots
     */
    private List<Ballot> getBallotsForPost(String postId) {
        // get ballots for post
        long start = System.nanoTime();
        List<Ballot> ballots = null;
        List<RequestHelper.StoredObject> objects = Storage.get(Admin.getAccessToken(), new String[] {"type", "properties.post_id"},
                new RequestHelper.Operation[] {RequestHelper.Operation.EQ, RequestHelper.Operation.EQ}, new String[] {"ballot", postId});
        long end = System.nanoTime();
        logger.info(String.format("Took %d milliseconds to GET ballots for postId %s", (end - start) / 1000000, postId));

        long s1 = System.nanoTime();

        // parse objects into ballots
        if (objects != null) {
            // build results
            ballots = new ArrayList<>();
            for (RequestHelper.StoredObject o : objects) {
                // deserialize into ballot
                Ballot ballot;
                try {
                    ballot = gson.fromJson(gson.toJson(o.getProperties()), Ballot.class);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                    continue;
                }

                // auto cast features (to integers)
                if (ballot.getFeatures() != null)
                    for (Feature f : ballot.getFeatures())
                        f.autoCast();

                // get tallier signatures
                HashMap<String, String> signatures = ballot.getSignatures();
                ballot.setSignatures(null);
                ballot.setProof(null);

                // verify tallier signatures
                if (signatures == null || signatures.size() < REQUIRED_SIGNATURES || !verifySignatures(ballot, signatures))
                    continue;

                // invalidate unwanted tally properties
                ballot.setPostId(null);

                // append to results
                ballots.add(ballot);
            }
        }

        long e1 = System.nanoTime();
        logger.info(String.format("Took %d milliseconds to VERIFY all ballots for postId %s", (e1 - s1) / 1000000, postId));

        return ballots;
    }

    /**
     * Verifies if the signatures of a ballot are valid.
     *
     * @param ballot ballot whose signatures are being verified
     * @param signatures map of ballot signatures
     * @return Boolean to determine of the signatures are valid
     */
    private boolean verifySignatures(Ballot ballot, HashMap<String, String> signatures) {
        // loop through signatures
        for (HashMap.Entry<String, String> s : signatures.entrySet()) {
            // get values
            String signer = s.getKey();
            String signature = s.getValue();

            // check cache
            if (!tallierCache.containsKey(signer))
                updateCaches();

            // get public key from cache
            if (!tallierCache.containsKey(signer)) return false;
            PublicKey key = tallierCache.get(signer).getPublicKey();

            // verify signature
            if (!Cryptography.RSA.verify(gson.toJson(ballot), signature, key))
                return false;
        }

        return true;
    }

    /**
     * Validates the signatures of a list of features against
     * the signing manager public key and the key image of the user.
     *
     * @param features list of features to be validated
     * @param keyImage key image of the user (sent in the PoK)
     * @return boolean to assess the validity of all features
     */
    private boolean validateFeatures(ArrayList<Feature> features, String keyImage) {
        // validate features
        for (Feature f : features) {
            // parse to integer if necessary
            f.autoCast();

            String message = gson.toJson(f.getFeature()) + keyImage;
            Map<String, String> signatures = f.getSignatures();

            // validate all signatures (only one)
            for (Map.Entry<String, String> s : signatures.entrySet()) {
                String signer = s.getKey();
                String signature = s.getValue();

                // check cache
                if (!managerCache.containsKey(signer))
                    updateCaches();

                // get public key from cache
                if (!managerCache.containsKey(signer)) return false;
                PublicKey key = managerCache.get(signer).getPublicKey();

                // verify signature
                if (!Cryptography.RSA.verify(message, signature, key))
                    return false;
            }
        }

        return true;
    }

    /**
     * Updates the local cache of certificates of managers
     * and talliers.
     */
    private void updateCaches() {
        // make call to discovery
        List<RequestHelper.StoredObject> services = Discovery.search(Admin.getAccessToken(), new String[] {"type"},
                new RequestHelper.Operation[] {RequestHelper.Operation.EQ}, new String[] {"voting"});
        if (services == null)
            return;

        for (RequestHelper.StoredObject s : services) {
            // get certificates for tallier and manager
            X509Certificate managerCertificate = Cryptography.X509.readCertificate(s.getProperties().get("manager").toString());
            X509Certificate tallierCertificate = Cryptography.X509.readCertificate(s.getProperties().get("tallier").toString());

            // save certificates on cache
            managerCache.putIfAbsent(s.getId(), managerCertificate);
            tallierCache.putIfAbsent(s.getId(), tallierCertificate);
        }
    }

}
