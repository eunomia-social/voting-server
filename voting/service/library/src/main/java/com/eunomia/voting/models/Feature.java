/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.models;

import java.util.Collections;
import java.util.Map;

/**
 * This class represents a single user feature retrieved from
 * the social network, which can be verified and appended
 * to ballots. To be valid, it must contain one or more
 * signatures.
 */
public class Feature {
    private Map<String, Object> feature;
    private Map<String, String> signatures;

    // TODO: timestamp?

    public Feature(Map<String, Object> feature) {
        if (feature.size() == 1)
            this.feature = feature;
    }

    public void setFeature(Map<String, Object> feature) {
        if (feature.size() == 1)
            this.feature = feature;
    }

    public Map<String, Object> getFeature() {
        return feature;
    }

    public Map<String, String> getSignatures() {
        return signatures;
    }

    public void setSignatures(Map<String, String> signatures) {
        this.signatures = signatures;
    }

    /**
     * Casts a feature which may be read as a double to
     * an integer.
     */
    public void autoCast() {
        if (!this.feature.isEmpty()) {
            Map.Entry<String, Object> entry = this.feature.entrySet().iterator().next();
            if (entry.getValue() instanceof Double)
                this.feature = Collections.singletonMap(entry.getKey(), ((Double) entry.getValue()).intValue());
        }
    }
}
