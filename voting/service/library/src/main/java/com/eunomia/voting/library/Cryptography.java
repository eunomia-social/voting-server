/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.library;

import kotlin.Pair;

import com.eunomia.voting.models.Proof;

import net.i2p.crypto.eddsa.math.GroupElement;
import net.i2p.crypto.eddsa.spec.EdDSANamedCurveTable;
import net.i2p.crypto.eddsa.spec.EdDSAParameterSpec;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.util.io.pem.PemReader;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

/**
 * The Cryptography class contains all the mathematical and cryptographic
 * operations required for the voting server to run successfully. It
 * implements symmetric and asymmetric cryptography with AES ({@link AES})
 * and RSA ({@link RSA}), various helper/utility function such as hashes
 * ({@link Utilities}), handles X.509 certificates ({@link X509}) and
 * has an implementation of elliptic curve cryptography ({@link ECC}) which
 * relies on the Edwards form of Curve25519.
 */
public class Cryptography {

    //################################################################################
    // RSA (PUBLIC KEY CRYPTOGRAPHY)
    //################################################################################

    /**
     * This class is responsible for handling all functionality
     * related to the asymmetric encryption algorithm RSA.
     */
    public static class RSA {

        //----------------------------------------------------------------------
        // KEYS & CERTIFICATES
        //----------------------------------------------------------------------

        /**
         * Size of the N (modulus) value defined when generating the RSA
         * keypair (in bits)
         */
        private static final Integer RSA_KEY_SIZE = 2048;

        /**
         * Validity of issued certificate (in days)
         */
        private static final Integer CERTIFICATE_VALIDITY = 365 * 10;

        /**
         * Generates a key pair and an X.509 certificate self-signed.
         *
         * @param commonName name used to generate the certificate
         * @return Pair with X.509 Certificate and a Private Key
         */
        public static Pair<X509Certificate, PrivateKey> generateCredentials(String commonName) {
            try {
                // generate 2048 RSA key pair
                KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
                kpg.initialize(RSA_KEY_SIZE);
                KeyPair keyPair = kpg.generateKeyPair();

                // set certificate dates (10-year validity)
                Instant now = Instant.now();
                Date notBefore = Date.from(now);
                Date notAfter = Date.from(now.plus(Duration.ofDays(CERTIFICATE_VALIDITY)));

                // set properties
                X500Name x500Name = new X500Name("CN=" + commonName);
                BigInteger serialNumber = BigInteger.valueOf(now.toEpochMilli());

                // generate certificate builder
                X509v3CertificateBuilder certificateBuilder = new JcaX509v3CertificateBuilder(x500Name, serialNumber, notBefore, notAfter, x500Name, keyPair.getPublic());

                // generate and self-sign certificate
                ContentSigner contentSigner = new JcaContentSignerBuilder("SHA256withRSA").build(keyPair.getPrivate());
                X509Certificate certificate = new JcaX509CertificateConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME)
                        .getCertificate(certificateBuilder.build(contentSigner));

                return new Pair<>(certificate, keyPair.getPrivate());
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        //----------------------------------------------------------------------
        // ENCRYPTION & DECRYPTION
        //----------------------------------------------------------------------

        /**
         * Auxiliary function ensure confidentiality of data (avoids duplicate code).
         * <p>It is used either to encrypt or decrypt data.
         * <p>It uses RSA with PKCS#1 v1.5 Padding.
         *
         * @param data data to be encrypted or decrypted
         * @param privateKey used to decrypt data (null if encrypting)
         * @param publicKey used to encrypt data (null if decrypting)
         * @param decrypt boolean to state whether the operation is a decryption
         * @return Data encrypted in base64 encoding or data decrypted
         *
         * @see #encrypt(String, PublicKey)
         * @see #decrypt(String, PrivateKey)
         */
        private static String confidentiality(String data, PrivateKey privateKey, PublicKey publicKey, boolean decrypt) {
            try {
                // create cipher
                Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

                // select mode of operation
                if (decrypt) {
                    cipher.init(Cipher.DECRYPT_MODE, privateKey);
                } else {
                    cipher.init(Cipher.ENCRYPT_MODE, publicKey);
                }

                // prepare data (base64 to bytes if decrypt)
                byte[] dataBytes = decrypt ? Base64.getDecoder().decode(data) : data.getBytes();

                // encrypt/decrypt data
                byte[] result = cipher.doFinal(dataBytes);

                // convert (string to base64 if encrypt) and return result
                return (decrypt ? new String(result) : Base64.getEncoder().encodeToString(result));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Encrypts data with the provided public key.
         * <p>Functionality provided by {@link #confidentiality(String, PrivateKey, PublicKey, boolean)}.
         *
         * @param data information to be encrypted
         * @param publicKey key used to encrypt
         * @return Encrypted data (base64)
         */
        public static String encrypt(String data, PublicKey publicKey) {
            return confidentiality(data, null, publicKey, false);
        }

        /**
         * Decrypts data with the provided private key.
         * <p>Functionality provided by {@link #confidentiality(String, PrivateKey, PublicKey, boolean)}.
         *
         * @param data information encrypted (base64)
         * @param privateKey key used to decrypt
         * @return Decrypted data
         */
        public static String decrypt(String data, PrivateKey privateKey) {
            return confidentiality(data, privateKey, null, true);
        }

        //----------------------------------------------------------------------
        // SIGNATURES
        //----------------------------------------------------------------------

        /**
         * Signs information using RSA/SHA256.
         *
         * @param data data to be signed (which is then converted to bytes)
         * @param privateKey key used to sign
         * @return Signed data in (base64)
         */
        public static String sign(String data, PrivateKey privateKey) {
            try {
                // create signature object (SHA256, PKCS#1 Padding, RSA)
                Signature s = Signature.getInstance("SHA256withRSA");

                // initialize signature
                s.initSign(privateKey);

                // prepare data
                byte[] bytes = data.getBytes();
                s.update(bytes);

                // sign data
                byte[] signature = s.sign();

                // convert to base64 and return result
                return Base64.getEncoder().encodeToString(signature);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Verifies data signed with RSA/SHA256.
         *
         * @param data data to be compared to the signature
         * @param signature signature to be verified (base64)
         * @param publicKey key used to verify the signature
         * @return Boolean to assess if the signature is valid
         */
        public static boolean verify(String data, String signature, PublicKey publicKey) {
            try {
                // create signature object
                Signature s = Signature.getInstance("SHA256withRSA");
                s.initVerify(publicKey);

                // add data to be verified
                s.update(data.getBytes());

                // convert signature (base64 to bytes)
                byte[] signatureBytes = Base64.getDecoder().decode(signature);

                // return result of verification
                return s.verify(signatureBytes);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    //################################################################################
    // AES (SYMMETRIC KEY CRYPTOGRAPHY)
    //################################################################################

    /**
     * Defines and implements all functionality regarding
     * symmetric encryption used in the project, specifically
     * Advanced Encryption Standard (AES).
     */
    public static class AES {

        //----------------------------------------------------------------------
        // KEYS & IVs
        //----------------------------------------------------------------------

        /**
         * Receives an AES key as string encoded in base64 and transforms
         * it into a key object. Can have any of the supported key sizes
         * but a 128 bit key is the standard in this library.
         *
         * @param key key to be read as string encoded (base64)
         * @return Key read or null if data is invalid
         */
        public static SecretKeySpec readKey(String key) {
            try {
                // key string to bytes
                byte[] keyBytes = Base64.getDecoder().decode(key);

                // return key spec
                return new SecretKeySpec(keyBytes, "AES");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Receives an Initialization Vector as string and encoded in
         * base64 and transforms it into an IV object.
         *
         * @param iv Initialization vector as string (base64)
         * @return IV read
         */
        public static IvParameterSpec readIV(String iv) {
            try {
                // iv string to bytes
                byte[] keyBytes = Base64.getDecoder().decode(iv);

                // return iv spec
                return new IvParameterSpec(keyBytes);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return null;
            }
        }

        //----------------------------------------------------------------------
        // ENCRYPTION & DECRYPTION
        //----------------------------------------------------------------------

        /**
         * Auxiliary function ensure confidentiality of data (avoids duplicate code).
         * <p>It is used either to encrypt or decrypt data.
         * <p>It uses AES with block mode Cipher Block Chaining (CBC), block and key
         * sizes of 128 bits.
         *
         * @param data data to be encrypted or decrypted
         * @param key symmetric key used to encrypt or decrypt data
         * @param iv initialization vector for CBC block mode
         * @param decrypt boolean to state whether the operation is a decryption
         * @return Data encrypted in base64 encoding or data decrypted
         *
         * @see #encrypt(String, SecretKeySpec)
         * @see #decrypt(String, SecretKeySpec, IvParameterSpec)
         */
        private static String confidentiality(String data, SecretKeySpec key, IvParameterSpec iv, boolean decrypt) {
            try {
                // define cipher (note: PKCS#5 padding is defined only for DES and is a
                // subtype of PKCS#7, so PKCS#5 can't even be applied to AES; in practical
                // terms, they are interchangeable)
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

                // init cipher and select mode of operation
                cipher.init(decrypt ? Cipher.DECRYPT_MODE : Cipher.ENCRYPT_MODE, key, iv);

                // prepare data (base64 to bytes if decrypt)
                byte[] dataBytes = decrypt ? Base64.getDecoder().decode(data) : data.getBytes();

                // encrypt/decrypt data
                byte[] result = cipher.doFinal(dataBytes);

                // convert (string to base64 if encrypt) and return result
                return (decrypt ? new String(result) : Base64.getEncoder().encodeToString(result));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Encrypts data using AES-CBC with provided symmetric key.
         * <p>Functionality provided by {@link #confidentiality(String, SecretKeySpec, IvParameterSpec, boolean)}.
         *
         * @param data information to be encrypted
         * @param key symmetric key used to encrypt
         * @return Encrypted data (base64)
         */
        public static Pair<String, IvParameterSpec> encrypt(String data, SecretKeySpec key) {
            // create secure random generator
            SecureRandom secureRandom = new SecureRandom();

            // generate iv of 16 bytes (same as AES block size)
            byte[] ivBytes = new byte[16];
            secureRandom.nextBytes(ivBytes);

            // init iv
            IvParameterSpec iv = new IvParameterSpec(ivBytes);

            // encrypt data
            String encrypted = confidentiality(data, key, iv, false);
            if (encrypted == null) return null;

            return new Pair<>(encrypted, iv);
        }

        /**
         * Decrypts data encrypted through AES-CBC with provided symmetric key.
         * <p>Functionality provided by {@link #confidentiality(String, SecretKeySpec, IvParameterSpec, boolean)}.
         *
         * @param data information encrypted (base64)
         * @param key symmetric key used to decrypt
         * @param iv initialization vector during the encryption process
         * @return Decrypted data
         */
        public static String decrypt(String data, SecretKeySpec key, IvParameterSpec iv) {
            return confidentiality(data, key, iv, true);
        }

    }

    //################################################################################
    // X.509 CERTIFICATES
    //################################################################################

    /**
     * Handles all functionality related to X.509 Certificates.
     */
    public static class X509 {

        //----------------------------------------------------------------------
        // CERTIFICATE TO/FROM STRING
        //----------------------------------------------------------------------

        /**
         * Reads a PEM X.509 certificate.
         *
         * @param pemCert X.509 certificate in PEM format
         * @return X.509 certificate as a java object
         */
        public static X509Certificate readCertificate(String pemCert) {
            // use bouncycastle to read pem string
            PemReader pemReader = new PemReader(new StringReader(pemCert));

            try {
                // parse certificate to bytes
                byte[] certBytes = pemReader.readPemObject().getContent();

                // generate certificate from bytes
                CertificateFactory certificateFactory = CertificateFactory.getInstance("X509");
                X509Certificate certificate = (X509Certificate)certificateFactory.generateCertificate(new ByteArrayInputStream(certBytes));

                // close reader
                pemReader.close();

                // return read certificate
                return certificate;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Prints X.509 certificate in PEM format.
         *
         * @param certificate certificate in X509Certificate object format
         * @return X.509 Certificate in PEM format
         */
        public static String printCertificate(X509Certificate certificate) {
            // string builder
            StringBuilder builder = new StringBuilder();

            try {
                // export certificate in base64
                String cert = new String(Base64.getEncoder().encode(certificate.getEncoded()));
                cert = cert.replaceAll("(.{64})", "$1\n");

                // build string
                builder.append("-----BEGIN CERTIFICATE-----\n");
                builder.append(cert);
                builder.append("\n-----END CERTIFICATE-----");
            } catch (CertificateEncodingException e) {
                e.printStackTrace();
                return null;
            }

            return builder.toString();
        }

    }

    //################################################################################
    // ELLIPTIC CURVE CRYPTOGRAPHY & ZERO-KNOWLEDGE PROOFS
    //################################################################################

    /**
     * Implements all functions related to elliptic curves used throughout
     * the project and to the zero-knowledge proofs used in the voting
     * protocol. All operations are executed on the (twisted Edwards)
     * Curve25519 and respective finite field.
     */
    public static class ECC {

        //----------------------------------------------------------------------
        // CURVE & KEYS
        //----------------------------------------------------------------------

        /**
         * Specification of Curve25519 in Twisted Edwards form (Ed25519)
         */
        private static final EdDSAParameterSpec curveSpec = EdDSANamedCurveTable.getByName(EdDSANamedCurveTable.ED_25519);

        /**
         * Order of the subgroup generated by the Ed25519 curve base point
         */
        private static final BigInteger SUBGROUP_ORDER = new BigInteger("2").pow(252)
                .add(new BigInteger("27742317777372353535851937790883648493"));

        /**
         * Generates a new group element given the point in compressed
         * format.
         *
         * @param point compressed point
         * @return Point on the group
         */
        private static GroupElement getPoint(String point) {
            byte[] bytes = Utilities.reverse(Base64.getDecoder().decode(point.getBytes(StandardCharsets.UTF_8)));
            return new GroupElement(curveSpec.getCurve(), bytes);
        }

        /**
         * Checks if a given point is valid within the Ed25519 curve.
         *
         * @param point point on the curve to be validated (256-bit value:
         *              bits 0-254 represent <b>y</b> coordinate and bit 255
         *              is the parity of the <b>x</b> coordinate, in little
         *              endian) (base64)
         * @return Boolean to assess if the point is valid and on the curve
         */
        public static boolean validatePoint(String point) {
            try {
                // build point and check if it is on curve
                GroupElement p = getPoint(point);
                return p.isOnCurve();
            } catch (Exception e) {
                return false;
            }
        }

        /**
         * Hashes a random string to a point in the subgroup generated
         * by the curve's base point (in the field). The point returned
         * already has precomputed doubles to allow fast double base
         * scalar multiplication.
         *
         * @param data data to be hashed
         * @return Point on the curve (with precomputed doubles)
         */
        public static GroupElement hashToPoint(String data) {
            GroupElement h;
            try {
                // hash data and convert to byte array
                String hash = Utilities.sha256(data);
                BigInteger exp = new BigInteger(1, Base64.getDecoder().decode(hash.getBytes(StandardCharsets.UTF_8))).mod(SUBGROUP_ORDER);
                byte[] expByte = Utilities.reverse(exp.toByteArray());

                // calculate point
                GroupElement p = curveSpec.getB().scalarMultiply(expByte);
                h = GroupElement.p3(curveSpec.getCurve(), p.getX(), p.getY(), p.getZ(), p.getT(), true);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            return h;
        }

        //----------------------------------------------------------------------
        // ZERO-KNOWLEDGE PROOFS
        //----------------------------------------------------------------------

        /**
         * Verifies the validity of the list of challenges generated and
         * sent by the voter. These challenges ensure the validity of, at
         * least, one of the presented statements in the proof.
         *
         * @param hash Hash of all the parameters (non-interactive challenge) (base64)
         * @param list List of all other challenges generated by the voter (base64)
         * @return Boolean to assess the validity of the challenges
         */
        private static boolean verifyChallenge(String hash, String[] list) {
            // convert all values to biginteger
            BigInteger challenge;
            List<BigInteger> challenges = new ArrayList<>();
            try {
                challenge = new BigInteger(1, Base64.getDecoder().decode(hash.getBytes(StandardCharsets.UTF_8))).mod(SUBGROUP_ORDER);
                Arrays.asList(list).forEach(c -> challenges.add(new BigInteger(1, Base64.getDecoder().decode(c.getBytes(StandardCharsets.UTF_8)))));
            } catch (Exception e) {
                return false;
            }

            // sum all challenges
            BigInteger sum = BigInteger.ZERO;
            for (BigInteger c : challenges) {
                sum = sum.add(c).mod(SUBGROUP_ORDER);
            }

            // compare values and return
            return challenge.equals(sum);
        }

        /**
         * Verifies the validity of the ring signature used to vote
         * or of the proof of knowledge for the key image.
         *
         * @param proof computed proof
         * @param postId identifier of the post the user is voting on (or
         *               null if validating a key image PoK)
         * @param message message signed (can be null)
         * @return Boolean to assess if the proof is valid
         */
        public static boolean verifyProof(Proof proof, String postId, String message) {
            // measure performance
            long start = System.nanoTime();

            try {
                // calculate proof size
                int proofSize = proof.getKeys().length;

                // compute non-interactive challenge
                String hash = Utilities.sha256(String.join("", proof.getKeyCommits()) + String.join("",
                        proof.getNymCommits()) + String.join("", proof.getKeys()) + proof.getNym() + (message == null ? "" : message));

                // verify challenges
                if (!verifyChallenge(hash, proof.getChallenges()))
                    return false;

                // hash postId to point
                GroupElement h = null;
                if (postId != null)
                    h = hashToPoint(postId);

                // get nym point
                GroupElement nym = getPoint(proof.getNym());

                // verify proof (key + nym)
                for (int i = 0; i < proofSize; i++) {
                    // convert parameters to byte array (challenge and response)
                    byte[] s = Utilities.reverse(Base64.getDecoder().decode((proof.getResponses()[i]).getBytes(StandardCharsets.UTF_8)));
                    byte[] c = Utilities.reverse(Base64.getDecoder().decode((proof.getChallenges()[i]).getBytes(StandardCharsets.UTF_8)));

                    ///// KEY PROOF /////

                    // get public key
                    GroupElement y = getPoint(proof.getKeys()[i]);

                    // recalculate h with a new public key
                    if (postId == null)
                        h = hashToPoint(proof.getKeys()[i]);

                    // calculate proof point
                    // must have P2 representation and base point precomputed
                    GroupElement pKey = y.negate().doubleScalarMultiplyVariableTime(curveSpec.getB(), s, c).toP2();

                    // get key commitment point
                    // must have P2 representation otherwise comparison might fail
                    GroupElement r = getPoint(proof.getKeyCommits()[i]).toP2();

                    // compare points
                    if (!r.equals(pKey)) return false;

                    ///// NYM PROOF /////

                    // calculate proof point
                    // must have P2 representation and h precomputed
                    GroupElement pNym = nym.negate().doubleScalarMultiplyVariableTime(h, s, c).toP2();

                    // get nym commitment point
                    // must have P2 representation otherwise comparison might fail
                    GroupElement R = getPoint(proof.getNymCommits()[i]).toP2();

                    // compare points
                    if (!R.equals(pNym)) return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }

            long end = System.nanoTime();
            System.out.printf("Took %d milliseconds to verify a proof with %d keys, for postId %s\n",
                    (end - start) / 1000000, proof.getKeys().length, postId);

            return true;
        }
    }

    //################################################################################
    // UTILITIES (HASHING, CONVERSIONS, ...)
    //################################################################################

    /**
     * Contains all auxiliary functionality to the cryptography part
     * of the project. Handles hash functions, message digests, data
     * conversions, etc.
     */
    public static class Utilities {

        //----------------------------------------------------------------------
        // CONVERSIONS (ENDIANNESS HELPER)
        //----------------------------------------------------------------------

        /**
         * Reverses the order of an array of bytes. Useful when
         * converting bit strings between big and little endian.
         * Also pads every array received to 32 bytes.
         *
         * @param array byte array to be reversed
         * @return 32-byte reversed byte array padded with 0s (if
         * necessary)
         */
        public static byte[] reverse(byte[] array) {
            // swap elements
            for (int i = 0; i < array.length / 2; i++) {
                byte t = array[i];
                array[i] = array[array.length - i - 1];
                array[array.length - i - 1] = t;
            }

            // fill array with 0s
            if (array.length < 32) {
                array = Arrays.copyOf(array, 32);
            }

            return array;
        }

        //----------------------------------------------------------------------
        // SHA-256
        //----------------------------------------------------------------------

        /**
         * Hashes information using the SHA256 function.
         *
         * @param data information to be hashed
         * @return Hashed data (base64)
         */
        public static String sha256(String data) {
            try {
                // hash message
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                md.update(data.getBytes());
                byte[] digest = md.digest();

                // convert to base64 and return digest
                return Base64.getEncoder().encodeToString(digest);
            } catch (Exception e) {
                // should not happen (algorithm is hardcoded)
                return null;
            }
        }
    }
}
