/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.library;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import okhttp3.HttpUrl;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;
import java.util.HashMap;

public class Authentication {

    /**
     * JSON parser
     */
    private static final Gson gson = new Gson();

    /**
     * Retrieves an EUNOMIA token for the voting server.
     *
     * @param identifierToken token that uniquely identifies the entity retrieving
     *                        the EUNOMIA token
     * @param authProvider uri of the authentication provider
     * @return Token retrieved or null if there was an error
     */
    public static String retrieveToken(String identifierToken, String authProvider) {
        // build url
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.NODE_SERVICES_URI + "/eunomia/token").newBuilder();
        urlBuilder.addQueryParameter("access_token", identifierToken);
        urlBuilder.addQueryParameter("auth_provider_uri", authProvider);
        String url = urlBuilder.build().toString();

        // execute request
        Response response = RequestHelper.execute(url, null, RequestHelper.Method.GET);
        try {
            // check if response is ok
            if (response == null || response.code() != 200) return null;

            // parse response body
            HashMap<String, Object> responseMap = gson.fromJson(response.body().string(), new TypeToken<HashMap<String, Object>>(){}.getType());
            HashMap<String, Object> dataMap = gson.fromJson(gson.toJson(responseMap.get("data")), new TypeToken<HashMap<String, Object>>(){}.getType());

            // assign access token
            String accessToken = dataMap.get("token").toString();

            // complete request
            if (accessToken != null) return accessToken;
        } catch (JsonSyntaxException | IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (response != null) response.body().close();
        }

        return null;
    }

    /**
     * Verifies if an EUNOMIA token is valid and associated
     * to a social network user identifier.
     *
     * @param token token to be verified
     * @param userId respective user identifier
     * @return Boolean to assess if the association is valid
     */
    public static boolean verifyUser(String token, String userId) {
        return handleToken(token, userId, false);
    }

    /**
     * Verifies if an EUNOMIA token exists and is valid.
     *
     * @param token token to be verified
     * @return Boolean to assess if the token is valid
     */
    public static boolean verifyToken(String token) {
        return handleToken(token, null, false);
    }

    /**
     * Revokes an existing and valid EUNOMIA token.
     *
     * @param token token to be revoked
     * @return Boolean to assess if the operation was successful
     */
    public static boolean revokeToken(String token) {
        return handleToken(token, null, true);
    }

    /**
     * Abstract method to verify or revoke an access token.
     *
     * @param token token to be handled
     * @param userId identifier of the user associated with the token (optional,
     *               null to verify just the token)
     * @param revoke boolean that defines the operation (true to revoke)
     * @return Boolean to assess if the operation was successful or if the token is valid
     *
     * @see #verifyToken(String)
     * @see #revokeToken(String)
     */
    private static boolean handleToken(String token, String userId, boolean revoke) {
        // confirm token exists before verifying/revoking
        if (token == null || token.isBlank())
            return false;

        // build url with parameters
        String path = revoke ? "/eunomia/token/revoke" : "/eunomia/token";
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.NODE_SERVICES_URI + path).newBuilder();
        urlBuilder.addQueryParameter("access_token", token);
        if (!revoke && userId != null) urlBuilder.addQueryParameter("user_id", userId);
        String url = urlBuilder.build().toString();

        // build empty request body
        RequestBody requestBody = RequestBody.create(new byte[0]);

        // execute request
        Response response = RequestHelper.execute(url, requestBody, RequestHelper.Method.POST);

        // verify if response got code 200
        return RequestHelper.verifyOk(response);
    }
}
