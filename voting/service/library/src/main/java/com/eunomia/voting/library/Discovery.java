/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.library;

import java.util.HashMap;
import java.util.List;

public class Discovery {

    /**
     * Saves services information on discovery service in this node, which
     * then spreads to other nodes.
     *
     * @param token token to access the discovery service
     * @param services hashmap that contains the services and corresponding characteristics
     * @param id identifier of the service (parent service, if multiple)
     * @param type type of service (parent service, if multiple)
     * @return Boolean to assess if the save operation was successful
     */
    public static boolean post(String token, HashMap<String, Object> services, String id, String type) {
        return RequestHelper.save(token, services, id, type, true, false);
    }

    /**
     * Extracts services information from discovery.
     *
     * @param token token to access the discovery service
     * @param properties list of properties to search for (id, type, ...)
     * @param ops list of operations to compare properties and values, check {@link RequestHelper.Operation}
     *            for possible values
     * @param values list of values to compare with the found properties' values
     * @return List of services that match the specified criteria
     */
    public static List<RequestHelper.StoredObject> search(String token, String[] properties, RequestHelper.Operation[] ops, String[] values) {
        return RequestHelper.get(token, properties, ops, values, true);
    }

    /**
     * Updates an existing service present the discovery database.
     *
     * @param token token to access the discovery service
     * @param services hashmap that contains the updated services
     * @param id identifier of the service to be updated (parent service, if multiple)
     * @param type type of service (parent service, if multiple)
     * @return Boolean to assess if the update operation was successful
     */
    public static boolean update(String token, HashMap<String, Object> services, String id, String type) {
        return RequestHelper.save(token, services, id, type, true, true);
    }

    /**
     * Removes a service from discovery.
     *
     * @param token token to access the discovery service
     * @param id identifier of the service to be deleted
     * @return Boolean to assess if the deletion operation was successful
     */
    public static boolean delete(String token, String id) {
        return RequestHelper.delete(token, id, true);
    }
}
