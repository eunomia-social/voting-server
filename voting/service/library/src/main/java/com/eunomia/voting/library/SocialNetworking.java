/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.library;

import com.eunomia.voting.models.Feature;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import okhttp3.HttpUrl;
import okhttp3.Response;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SocialNetworking {
    private static boolean enable=false;
    
    /**
     * Logger
     */
    private static Logger logger = LogManager.getLogger(SocialNetworking.class);
    /**
     * JSON Parser
     */
    private static final Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    /**
     * Enum to define which social networking platform EUNOMIA is
     * using.
     */
    public enum Platform {
        MASTODON("/api/v1/accounts/",
                "/api/v1/statuses/",
                "followers_count",
                "mastodonId");

        /**
         * Path to retrieve information from the social network account
         */
        public final String accountPath;

        /**
         * Path to access post information on the social network
         */
        public final String postsPath;

        /**
         * Key in the response to get the number of followers
         */
        public final String followersKey;

        /**
         * Name of the key in the storage server object which value is
         * the social network identifier for the user
         */
        public final String storageIdKey;

        Platform(String accountPath, String postsPath, String followersKey, String storageIdKey) {
            this.accountPath = accountPath;
            this.postsPath = postsPath;
            this.followersKey = followersKey;
            this.storageIdKey = storageIdKey;
        }
    }

    //------------------------------------------------------------------------------------------
    // POSTS
    //------------------------------------------------------------------------------------------

    /**
     * Verifies if a specific social network post exists.
     *
     * @param platform selects the social network used with EUNOMIA
     * @param authProvider endpoint for the social network API
     * @param postId identifier of the post
     * @return Boolean to assess if such post exists
     */
    public static boolean verifyPost(Platform platform, String authProvider, String postId) {
        if(!isEnable()){
            logger.warn("verifyPost() is disabled");
            return true;
        }
        // verify basic conditions
        if (postId == null || postId.isBlank())
            return false;
        //check if this is a global post id federated address, if it contains a '@' then it is.
        if(postId.contains("@")){ 
            String[] toks=postId.split("@");
            postId=toks[0];
            authProvider=toks[1];
        }
        
        logger.info("verifyPost() platform:"+platform+" authProvider:"+authProvider+" postId:"+postId);
        // build url for post verification
        String url;
        try {
            HttpUrl.Builder urlBuilder = HttpUrl.parse("https://" + authProvider + platform.postsPath).newBuilder();
            urlBuilder.addPathSegment(postId);
            url = urlBuilder.build().toString();
        } catch (Exception e) {
            logger.warn("verifyPost() platform:"+platform+" authProvider:"+authProvider+" postId:"+postId,e);
            return false;
        }

        // check if post exists (HTTP code == 200)
        Response response=RequestHelper.execute(url, null, RequestHelper.Method.GET);
        logger.info("verifyPost() Remote social network returned:"+response+" for GET "+url);
        return RequestHelper.verifyOk(response);
    }

    //------------------------------------------------------------------------------------------
    // USER FEATURES
    //------------------------------------------------------------------------------------------

    /**
     * Retrieves user features from social networking platform directly.
     * As of now, it only retrieves the number of followers.
     *
     * @param platform selects the social network used with EUNOMIA
     * @param authProvider endpoint for the social network API
     * @param token token to access social network information
     * @param userId identifier of the user for whom the features are
     *               being extracted
     * @return Array of unsigned features
     */
    public static ArrayList<Feature> getFeatures(Platform platform, String authProvider, String token, String userId) {
        // create list of features (and then append)
        ArrayList<Feature> features = new ArrayList<>();
        if(!isEnable()){
            logger.warn("getFeatures() is disabled");
            return features;
        }
        // get social network id from storage
        List<RequestHelper.StoredObject> users = Storage.get(token, new String[] {"id"},
                new RequestHelper.Operation[] {RequestHelper.Operation.EQ}, new String[] {userId});
        if (users == null || users.size() < 1)
            return features;

        // identifier of the user in the social network
        String socialNetworkId = users.get(0).getProperties().get(platform.storageIdKey).toString();

        // build request to social network
        String url;
        try {
            HttpUrl.Builder urlBuilder = HttpUrl.parse("https://" + authProvider + platform.accountPath).newBuilder();
            urlBuilder.addPathSegment(socialNetworkId);
            url = urlBuilder.build().toString();
        } catch (Exception e) {
            e.printStackTrace();
            return features;
        }

        // execute request
        Response response = RequestHelper.execute(url, null, RequestHelper.Method.GET);

        // parse body
        try {
            // check if response is ok
            if (response == null || response.code() != 200)
                return null;

            // parse response
            HashMap<String, Object> map = gson.fromJson(response.body().string(), new TypeToken<HashMap<String, Object>>(){}.getType());
            Integer followers = ((Double) map.get(platform.followersKey)).intValue();

            // append number of followers
            features.add(new Feature(Collections.singletonMap("followers", followers)));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            // close body to avoid connection leak
            if (response != null) response.body().close();
        }

        return features;
    }

    /**
     * @return the enable
     */
    public static boolean isEnable() {
        return enable;
    }

    /**
     * @param aEnable the enable to set
     */
    public static void setEnable(boolean aEnable) {
        enable = aEnable;
    }
}
