/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.library;

import com.eunomia.voting.models.Message;

import com.google.gson.Gson;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class Communication {

    /**
     * JSON parser
     */
    private static final Gson gson = new Gson();

    /**
     * Cache of nodes that maps voting server identifiers to
     * node identifiers
     */
    private static final ConcurrentHashMap<String, String> nodeCache = new ConcurrentHashMap<>();

    //------------------------------------------------------------------------------------------
    // BINDING/UNBINDING & NODE MAPPING
    //------------------------------------------------------------------------------------------

    /**
     * Subscribes a service to the node's communication service.
     *
     * @param token token to access the communication service
     * @param id general identifier of the service registering with
     *           the communication service (in this case, "voting")
     * @param name name of the service being registered (identifier
     *             of the service)
     * @param callback url to be called when delivering messages
     * @return Boolean to assess if the bind was successful
     */
    public static boolean bind(String token, String id, String name, String callback) {
        // build url
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.NODE_SERVICES_URI + "/communication/bind").newBuilder();
        urlBuilder.addQueryParameter("access_token", token);
        String url = urlBuilder.build().toString();

        // build request body
        HashMap<String, Object> body = new HashMap<>();
        body.put("id", id);
        body.put("name", name);
        body.put("callback", callback);

        // convert to json and create body object
        String jsonObject = gson.toJson(body);
        RequestBody requestBody = RequestBody.create(jsonObject, MediaType.parse("application/json"));

        // execute request
        Response response = RequestHelper.execute(url, requestBody, RequestHelper.Method.POST);

        // verify if response got code 200
        return RequestHelper.verifyOk(response);
    }

    /**
     * Unbinds a service from communication service.
     *
     * @param token token to access the communication service
     * @param id identifier of the service unbinding from the
     *           communication service
     * @return Boolean to assess if the unbind was successful
     */
    public static boolean unbind(String token, String id) {
        // build url
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.NODE_SERVICES_URI + "/communication/unbind").newBuilder();
        urlBuilder.addQueryParameter("access_token", token);
        urlBuilder.addQueryParameter("service_id", id);
        String url = urlBuilder.build().toString();

        // execute request
        Response response = RequestHelper.execute(url, null, RequestHelper.Method.DELETE);

        // verify if response got code 200
        return RequestHelper.verifyOk(response);
    }

    /**
     * Maps a voting server to the corresponding node and gets
     * the identifier of the node.
     *
     * @param token token to access the discovery service
     * @param votingId identifier of this voting service
     * @return Identifier of the node running that voting service
     */
    public static String mapNode(String token, String votingId) {
        // check cache
        if (nodeCache.containsKey(votingId))
            return nodeCache.get(votingId);

        // discovery call
        List<RequestHelper.StoredObject> services = Discovery.search(token, new String[] {"type", "properties.VOTING_SERVER_ID"},
                new RequestHelper.Operation[] {RequestHelper.Operation.EQ, RequestHelper.Operation.EQ}, new String[] {"node", votingId});

        // check if node exists
        if (services == null || services.isEmpty()) return null;

        // save on cache
        String nodeId = services.get(0).getId();
        nodeCache.put(votingId, nodeId);

        // get node id (first element)
        return nodeId;
    }

    //------------------------------------------------------------------------------------------
    // MESSAGING
    //------------------------------------------------------------------------------------------

    /**
     * Sends a message to a voting service of another node.
     *
     * @param token token to access the communication service
     * @param sender identifier of the voting server sending the
     *               message
     * @param receivers list of node identifiers that receive the
     *                  message
     * @param message message to be delivered to the receivers
     * @return Boolean to assess if the sending was successful
     */
    public static boolean send(String token, String sender, String[] receivers, Message message) {
        // build url
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.NODE_SERVICES_URI + "/communication/send").newBuilder();
        urlBuilder.addQueryParameter("access_token", token);
        String url = urlBuilder.build().toString();

        // build request body
        HashMap<String, Object> body = new HashMap<>();
        body.put("sender", sender);
        body.put("content", gson.toJson(message));

        // add receivers
        List<HashMap<String, String>> nodes = new ArrayList<>();
        for (String node : receivers) {
            // create receiver object
            HashMap<String, String> receiver = new HashMap<>();
            receiver.put("node_id", node);
            receiver.put("service_id", Constants.SERVICE_NAME);

            // add to list of receivers
            nodes.add(receiver);
        }

        // add nodes to request
        body.put("receivers", nodes);

        // convert to json and create body object
        String jsonObject = gson.toJson(body);
        RequestBody requestBody = RequestBody.create(jsonObject, MediaType.parse("application/json"));

        // execute request
        Response response = RequestHelper.execute(url, requestBody, RequestHelper.Method.POST);

        // verify if response got code 200
        return RequestHelper.verifyOk(response);
    }

    /**
     * Broadcasts a message to a random set of nodes, usually expecting
     * a response from some or all of them.
     *
     * @param token token to access the communication service
     * @param sender identifier of the voting server sending the message
     * @param limit number of nodes to send the message to
     * @param message message to be broadcast to the other voting servers
     * @return Boolean to assess if the broadcast was successful
     */
    public static boolean broadcast(String token, String sender, int limit, Message message) {
        // check limit
        if (limit < 1) return true;

        // create list of receivers
        List<String> receivers = new ArrayList<>();

        // search for nodes
        List<RequestHelper.StoredObject> services = Discovery.search(token, new String[] {"type", "properties.VOTING_SERVER_ID"},
                new RequestHelper.Operation[] {RequestHelper.Operation.EQ, RequestHelper.Operation.NE}, new String[] {"node", sender});
        if (services == null || services.size() == 0)
            return false;

        // compare sizes
        if (limit >= services.size()) {
            // add all nodes
            services.forEach(service -> receivers.add(service.getId()));
        } else {
            // select random nodes
            for (int i = 0; i < limit; i++) {
                // random node
                int index = new Random().nextInt(services.size());
                RequestHelper.StoredObject node = services.get(index);

                // add node id
                receivers.add(node.getId());

                // remove from services
                services.remove(index);
            }
        }

        // send to all receivers
        return send(token, sender, receivers.toArray(new String[0]), message);
    }
}
