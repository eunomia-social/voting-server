/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.library;

import okhttp3.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import java.net.ConnectException;

public class RequestHelper {

    /**
     * JSON Parser
     */
    private static final Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    //------------------------------------------------------------------------------------------
    // HTTP REQUESTS
    //------------------------------------------------------------------------------------------

    /**
     * List of HTTP methods
     */
    public enum Method {
        GET, POST, PUT, DELETE
    }

    /**
     * OkHttpClient used to execute requests
     */
    private static final OkHttpClient client = new OkHttpClient();

    /**
     * Executes an HTTP request.
     *
     * @param url address of the service, with query and path parameters
     * @param body body of the request
     * @param method type of request, check {@link Method} for possible methods
     * @return Response after request execution or null if failed
     */
    public static Response execute(String url, RequestBody body, Method method) {
        // prepare response and retrials
        Response response = null;
        int retrials = Constants.SERVER_RETRIALS;
        boolean execute = true;

        // retry a few times until success
        while (execute) {
            // create response builder
            Request.Builder builder;
            switch (method) {
                case GET:
                    builder = new Request.Builder().url(url).get();
                    break;
                case POST:
                    builder = new Request.Builder().url(url).post(body);
                    break;
                case PUT:
                    builder = new Request.Builder().url(url).put(body);
                    break;
                case DELETE:
                    builder = new Request.Builder().url(url).delete();
                    break;
                default:
                    return null;
            }

            // add content type and build
            Request request = builder.build();

            // execute call and get response
            try {
                response = client.newCall(request).execute();
                execute = false;
            } catch (ConnectException e1) {
                // check retrial state
                if (retrials == 0) {
                    System.out.println("Connection failed... giving up trying to connect to " + url);
                    return null;
                }

                // update retrials
                System.out.println("Connection failed... sleeping for " + Constants.SERVER_SLEEP_RETRY + "ms and will try again "
                        + retrials + " times...");
                retrials -= 1;

                try {
                    Thread.sleep(Constants.SERVER_SLEEP_RETRY);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        return response;
    }

    /**
     * Verifies if the response received has HTTP status code
     * 200 (successful)
     *
     * @param response HTTP response from a request
     * @return Boolean to assess if response got code 200
     */
    public static boolean verifyOk(Response response) {
        // return true if ok (close body to avoid connection leak)
        try {
            if (response != null && response.code() == 200) return true;
        } finally {
            if (response != null) response.body().close();
        }

        return false;
    }

    //------------------------------------------------------------------------------------------
    // VOTING SERVER RESPONSE HANDLING
    //------------------------------------------------------------------------------------------

    /**
     * This class declares standard error messages for all the Voting
     * Server operations.
     */
    public static class ErrorMessage {
        public static final String BAD_REQUEST = "Request is malformed";
        public static final String VOTER = "Error occurred during a voter operation";
        public static final String BALLOT = "Error occurred while generating ballot";
        public static final String FEATURES = "Error occurred while getting user features";
        public static final String ELIGIBILITY = "Error occurred during the eligibility request";
        public static final String DELETION = "Error occurred during the ballot deletion process";
        public static final String VOTING = "Error occurred during the voting process";
        public static final String ANONYMIZATION = "Error occurred during the anonymization process";
        public static final String TALLY = "Error occurred during the tallying process";
    }

    /**
     * Builds a standardized response to be returned to the requester.
     * It structures the response as a hashmap with the keys <b>message</b>,
     * <b>data</b> and <b>error</b>. Additionally, it adds a timestamp to
     * the response for the requester to know when it was generated.
     *
     * @param message Message to be shown to the requester (in any case, error or success)
     * @param data Data that the request generates (null in case of error)
     * @param error Error that the Voting Server encountered during the call (null in
     *              case of success)
     * @return Response as JSON String
     */
    public static String buildResponse(String message, Object data, String error) {
        // response object
        HashMap<String, Object> response = new HashMap<>();
        if (message != null) response.put("message", message);

        if (error == null) {
            if (data != null) response.put("data", data);
        } else {
            response.put("error", error);
        }

        // add timestamp
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Instant now = timestamp.toInstant();
        response.put("timestamp", now.toString());

        // convert to json
        return gson.toJson(response);
    }

    //------------------------------------------------------------------------------------------
    // STORAGE & DISCOVERY
    //------------------------------------------------------------------------------------------

    /**
     * List of possible operators for storage/discovery search
     */
    public enum Operation {
        EQ, NE, GT, LT, GTE, LTE, MOD
    }

    /**
     * Accesses storage/discovery and saves or updates objects/services.
     *
     * @param token token to access the specified service
     * @param obj object to be saved (hashmap for services)
     * @param id (only for discovery) identifier of the service (parent service, if multiple)
     * @param type type of object/service
     * @param service boolean to define whether to access storage or discovery
     * @param update boolean to set if the operation is an object/service update
     * @return Boolean to assess if the save operation was successful
     *
     * @see Discovery#post(String, HashMap, String, String)
     * @see Discovery#update(String, HashMap, String, String)
     * @see Storage#save(String, Object, String)
     * @see Storage#update(String, Object, String, String)
     */
    protected static boolean save(String token, Object obj, String id, String type, boolean service, boolean update) {
        // build url
        String path = service ? "/discovery" : "/objects";
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.NODE_SERVICES_URI + path).newBuilder();
        if (update) urlBuilder.addPathSegment(id);
        urlBuilder.addQueryParameter("access_token", token);
        String url = urlBuilder.build().toString();

        // build request body
        HashMap<String, Object> body = new HashMap<>();
        body.put("properties", obj);
        body.put("type", type);
        if (service) body.put("id", id);

        String jsonObject = gson.toJson(body);
        RequestBody requestBody = RequestBody.create(jsonObject, MediaType.parse("application/json"));

        // execute request and get response
        Response response = execute(url, requestBody, (update ? Method.PUT : Method.POST));

        // verify if response got code 200
        return verifyOk(response);
    }

    /**
     * Accesses storage/discovery and extracts objects/services.
     *
     * @param token token to access the specified service
     * @param properties list of properties to search for (id, type, ...)
     * @param ops list of operations to compare properties and values, check{@link RequestHelper.Operation}
     *            for possible values
     * @param values list of values to compare with the found properties' values
     * @param service boolean to define whether to access storage or discovery
     * @return List of services/objects that match the specified criteria
     *
     * @see Discovery#search(String, String[], Operation[], String[])
     * @see Storage#get(String, String[], Operation[], String[])
     */
    protected static List<RequestHelper.StoredObject> get(String token, String[] properties, Operation[] ops, String[] values, boolean service) {
        // build base url
        String path = service ? "/discovery" : "/objects";
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.NODE_SERVICES_URI + path).newBuilder();
        urlBuilder.addQueryParameter("access_token", token);

        // add query parameter for each property
        Arrays.asList(properties).forEach(property -> urlBuilder.addQueryParameter("property", property));
        Arrays.asList(ops).forEach(op -> urlBuilder.addQueryParameter("comp", op.name().toLowerCase()));
        Arrays.asList(values).forEach(value -> urlBuilder.addQueryParameter("value", value));

        // build url
        String url = urlBuilder.build().toString();

        // get response body
        Response response = execute(url, null, Method.GET);

        // return variable
        List<RequestHelper.StoredObject> result = new ArrayList<>();

        // parse body
        try {
            // check if response is ok
            if (response == null || response.code() != 200) return null;

            // parse response
            HashMap<String, Object> map = gson.fromJson(response.body().string(), new TypeToken<HashMap<String, Object>>(){}.getType());
            List<Object> data;

            if (service) {
                HashMap<String, Object> entitiesMap = gson.fromJson(gson.toJson(map.get("data")),
                        new TypeToken<HashMap<String, Object>>(){}.getType());
                data = gson.fromJson(gson.toJson(entitiesMap.get("entities")), new TypeToken<List<Object>>(){}.getType());
            } else {
                data = gson.fromJson(gson.toJson(map.get("data")), new TypeToken<List<Object>>(){}.getType());
            }

            // get each object from "data" response key
            for (Object o : data) {
                // deserialize json into storageObject
                RequestHelper.StoredObject obj = gson.fromJson(gson.toJson(o), RequestHelper.StoredObject.class);

                // deserialize object properties
                HashMap<String, Object> prop = gson.fromJson(gson.toJson(obj.getProperties()),
                        new TypeToken<HashMap<String, Object>>(){}.getType());
                obj.setProperties(prop);

                // append storageObject to return list
                result.add(obj);
            }
        } catch (JsonSyntaxException | IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            // close body to avoid connection leak
            if (response != null) response.body().close();
        }

        return result;
    }

    /**
     * Accesses storage/discovery and deletes objects/services.
     *
     * @param token token to access the specified service
     * @param id identifier of the object being deleted
     * @param service boolean to define whether to access storage or discovery
     * @return Boolean to assess if the deletion operation was successful
     *
     * @see Discovery#delete(String, String)
     * @see Storage#delete(String, String)
     */
    protected static boolean delete(String token, String id, boolean service) {
        // set url path
        String path = service ? "/discovery" : "/objects";

        // build url with parameters
        HttpUrl.Builder urlBuilder = HttpUrl.parse(Constants.NODE_SERVICES_URI + path).newBuilder();
        urlBuilder.addPathSegment(id);
        urlBuilder.addQueryParameter("access_token", token);
        String url = urlBuilder.build().toString();

        // execute request
        Response response = execute(url, null, Method.DELETE);

        // verify if response got code 200
        return verifyOk(response);
    }

    //------------------------------------------------------------------------------------------
    // OBJECTS TO DESERIALIZE FROM JSON
    //------------------------------------------------------------------------------------------

    /**
     * Representation of an object returned by the storage or discovery service
     */
    public static class StoredObject {
        private String id;
        private HashMap<String, Object> properties;
        private String type;

        public String getId() {
            return id;
        }

        public void setProperties(HashMap<String, Object> properties) {
            this.properties = properties;
        }

        public HashMap<String, Object> getProperties() {
            return properties;
        }
    }
}
