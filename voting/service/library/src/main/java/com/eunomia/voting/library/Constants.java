/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.library;

/**
 * Constants used to set up the voting server.
 */
public final class Constants {
    public static final String SERVICE_NAME = "voting";
    public static final String API_BASE_URI = "http://0.0.0.0/";
    public static final Integer PORT_NUMBER = 8100;
    public static final String API_PATH = "/api";
    public static final Integer SERVER_RETRIALS = 3;
    public static final Long SERVER_SLEEP_RETRY = 3000L;
    public static final String NODE_SERVICES_URI = "http://storage-server:5000/api";
    public static final String COMMUNICATION_CALLBACK = "http://voting-server:8100/api/voting/receive";
    public static final String VOTING_ENV_VAR = "VOTING_ID";
    public static final String TOKEN_ENV_VAR = "VOTING_TOKEN";
    public static final String SOCIAL_NETWORK_ENV_VAR = "SOCIAL_NETWORK";
    public static final String AUTH_ENV_VAR = "AUTH_PROVIDER";
    public static final String SIGNATURES_ENV_VAR = "REQUIRED_SIGNATURES";
    public static final String BASE64_REGEX = "([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?";
}