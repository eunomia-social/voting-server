/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Defines a ballot object recognized within the entire EUNOMIA
 * ecosystem.
 */
public class Ballot {
    @SerializedName("post_id")
    private String postId;

    private HashMap<String, Integer> votes;
    private ArrayList<Feature> features;

    @SerializedName("feature_proof")
    private Proof featureProof;

    private Proof proof;
    private HashMap<String, String> signatures;

    public Ballot(String postId, HashMap<String, Integer> votes) {
        this.postId = postId;
        this.votes = votes;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public HashMap<String, Integer> getVotes() {
        return votes;
    }

    public void setVotes(HashMap<String, Integer> votes) {
        this.votes = votes;
    }

    public ArrayList<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(ArrayList<Feature> features) {
        this.features = features;
    }

    public Proof getFeatureProof() {
        return featureProof;
    }

    public void setFeatureProof(Proof featureProof) {
        this.featureProof = featureProof;
    }

    public Proof getProof() {
        return proof;
    }

    public void setProof(Proof proof) {
        this.proof = proof;
    }

    public HashMap<String, String> getSignatures() {
        return signatures;
    }

    public void setSignatures(HashMap<String, String> signatures) {
        this.signatures = signatures;
    }
}
