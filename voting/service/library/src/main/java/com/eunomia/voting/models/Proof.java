/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.models;

import com.google.gson.annotations.SerializedName;

/**
 * Defines a proof object which proves the validity of a
 * pseudonym or a key image, in a ring signature format.
 */
public class Proof {
    @SerializedName("key_commits")
    private String[] keyCommits;

    @SerializedName("nym_commits")
    private String[] nymCommits;

    private String nym;
    private String[] challenges;
    private String[] responses;
    private String[] keys;

    public String[] getKeyCommits() {
        return keyCommits;
    }

    public String[] getNymCommits() {
        return nymCommits;
    }

    public String getNym() {
        return nym;
    }

    public String[] getChallenges() {
        return challenges;
    }

    public String[] getResponses() {
        return responses;
    }

    public String[] getKeys() {
        return keys;
    }

    public boolean hasValidFormat() {
        return keyCommits != null && nymCommits != null && nym != null
                && challenges != null && responses != null && keys != null;
    }
}