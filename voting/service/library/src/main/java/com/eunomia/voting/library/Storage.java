/** 
 * Copyright (C) 2018 INOV – Instituto de Engenharia de Sistemas e Computadores Inovacao  (https://www.inov.pt/)
 * 
 * Licensed under the EUPL, Version 1.2.
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * 
 *  https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and limitations under the Licence.
 */
package com.eunomia.voting.library;

import java.util.List;

public class Storage {
    /**
     * Saves object on node local storage (which is then synchronized
     * across all nodes).
     *
     * @param token token to access the storage service
     * @param obj object to be saved in the storage service
     * @param type type of object (can be anything, e.g. ballot)
     * @return Boolean to assess if the save operation was successful
     */
    public static boolean save(String token, Object obj, String type) {
        return RequestHelper.save(token, obj, null, type, false, false);
    }

    /**
     * Extracts objects from storage (locally, but globally synchronized).
     *
     * @param token token to access the storage service
     * @param properties list of properties to search for (id, type, ...)
     * @param ops list of operations to compare properties and values, check
     *            {@link RequestHelper.Operation} for possible values
     * @param values list of values to compare with the found properties' values
     * @return List of objects that match the specified criteria
     */
    public static List<RequestHelper.StoredObject> get(String token, String[] properties, RequestHelper.Operation[] ops, String[] values) {
        return RequestHelper.get(token, properties, ops, values, false);
    }

    /**
     * Updates an object living on the node's local storage (which is then
     * synchronized across all nodes).
     *
     * @param token token to access the storage service
     * @param obj object to be updated in the storage service
     * @param id identifier of the object being updated
     * @param type type of object (can be anything, e.g. ballot)
     * @return Boolean to assess if the update operation was successful
     */
    public static boolean update(String token, Object obj, String id, String type) {
        return RequestHelper.save(token, obj, id, type, false, true);
    }

    /**
     * Removes an object from the node storage.
     *
     * @param token token to access the storage service
     * @param id identifier of the object to be deleted
     * @return Boolean to assess if the deletion operation was successful
     */
    public static boolean delete(String token, String id) {
        return RequestHelper.delete(token, id, false);
    }
}